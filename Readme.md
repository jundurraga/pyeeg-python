# WE CHANGED NAME AND MOVED! 
We are now pEEGy. Plese use the new repository.

https://gitlab.com/jundurraga/peegy/

# PYEEG-Python

Tools for analysing EEG data in python, compatible with BIOSEMI databases.

The PYEEG steps are basically:
1) Load BDF/EDF files (raw data)
2) Define your pipeline for EEG-Analysis (Referencing EEG using predefinded EEG Cap Layouts,
 creating Epochs based on trigger events, downsampling, removing artifacts, defining Regions
 of Interest, Statistics to run on the epochs, or plot the data)
3) Define in which database you want to save the results
4) Run the above steps.

Examples can be found in https://jundurraga.gitlab.io/pyeeg-python/

## Installation
This software requires at least Python 3.7
For additional package installations, pip and Git are needed.

Then just perform a git clone in the directory of your choice:
git clone https://gitlab.com/jundurraga/pyeeg-python.git

This will clone into the folder 'pyeeg-python'.

To be able to look in the (generated) sql-databases, install DB Browser (https://sqlitebrowser.org/)

### For Windows
Precompiled PyFFTW is easier to install via conda.
If you are using window, this is the easiest way to have PyFFT running.
- conda install -c conda-forge pyfftw

### Installation with pip
pyeeg-pyhon can by installed by running
- python -m pip install -e ./

### pyeeg-python in PyCharm
* Open a new project and select the clonded PYEEG-Python folder.
* Go to (Settings -> Project: Project Interpreter, Python 3.7) and add a Virtualenv Environment for this project (to keep Package 
installations separate from other projects, which might require different versions than this).
Under Settings -> Tools -> Python Integrated Tools make sure that 'requirement.txt' is set as Package requirement file.
* If you now open one of the examples in the examples folder, PyCharm should aks you to install the requirements (this might take some while,
and make sure to resolve all errors). If you get errors with existing package installations, it might be worthwhile to try out the --ignore-install option (when running from terminal).

## Examples
Please have a look at the 'examples' folder for different EEG analysis examples in the time or frequency domain.
