from pyeeg.processing.pipe.definitions import InputOutputProcess
from pyeeg.processing.tools.epochs_processing_tools import et_subtract_oeg_template, et_subtract_correlated_ref, \
    filt_filt_data, et_get_spatial_filtering, et_apply_spatial_filtering, et_mean, et_frequency_mean2, \
    et_average_frequency_transformation, et_average_time_frequency_transformation, \
    et_impulse_response_artifact_subtraction, et_snr_in_rois
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
import pyeeg.processing.tools.filters.eegFiltering as eegf
import multiprocessing
from pyeeg.processing.tools.eeg_epoch_operators import et_unfold
from pyeeg.processing.tools.peak_detection.time_domain_tools import detect_peaks_and_amplitudes, TimePeakWindow, \
    PeakToPeakMeasure
from pyeeg.processing.tools.roi.definitions import TimeROI, Marker
from pyeeg.definitions.eeg_definitions import EegPeak
from pyeeg.plot import eeg_ave_epochs_plot_tools as eegpt
from pyeeg.definitions.channel_definitions import Domain, ChannelItem
from pyeeg.definitions.events import SingleEvent, Events
from pyeeg.processing.statistics.definitions import PhaseLockingValueTest, HotellingTSquareFrequencyTest, \
    FrequencyFTest, FpmTest
from pyeeg.processing.tools.epochs_processing_tools import et_ica_epochs
from pyeeg.processing.tools.eeg_epoch_operators import et_fold, w_mean
import pyfftw
from pyeeg.processing.statistics.eeg_statistic_tools import hotelling_t_square_test, phase_locking_value
import numpy as np
import pandas as pd
from sklearn.decomposition import FastICA
from sklearn.cross_decomposition import CCA
import itertools
from scipy.stats import f
import os
import copy
from matplotlib.ticker import FormatStrFormatter
from pyeeg.processing.tools.multiprocessing.multiprocessesing_filter import filt_data
import graphviz
import astropy.units as u
from pyeeg.tools.units.unit_tools import set_default_unit
import gc
import PIL as pil
import pydot
import tempfile
from PyQt5.QtCore import QLibraryInfo
os.environ["QT_QPA_PLATFORM_PLUGIN_PATH"] = QLibraryInfo.location(QLibraryInfo.PluginsPath)


class PipeItem:
    def __init__(self, name='', process: InputOutputProcess = None):
        self.name = name
        self.process = process
        self.process.name = name
        self.ready = False
        self.figure_handle = None


class PipePool(list):
    """This class manages InputOutputProcess appended to it.
    When InputOutputProcess are appended, to the PipePool, they can be called by calling the run function of the
    PipePool. If an element has been already run, this won't be called again.
    """

    def __init__(self):
        super(PipePool, self).__init__()

    def append(self, item: object, name=None):
        if name is None:
            name = type(item).__name__
        super(PipePool, self).append(PipeItem(**{'name': name,
                                                 'process': item}))

    def __getitem__(self, key):
        return super(PipePool, self).__getitem__(key)

    def get_process(self, value):
        out = None
        for _i, _item in enumerate(self):
            if _item.name == value:
                out = _item.process
                break
        return out

    def run(self):
        for _pipe_item in self:
            if _pipe_item.ready:
                continue
            _pipe_item.process.run()
            _pipe_item.ready = True

    def diagram(self,
                file_name: str = '',
                return_figure=False,
                dpi=300,
                ):
        """
        This function generates a flow diagram with the available connections in the pipeline
        :param file_name: string indicating the file name to save pipeline diagram
        :param return_figure: boolean indicating if figure should be returned

        """
        _file, file_extension = os.path.splitext(file_name)
        _temp_file = tempfile.mkdtemp() + os.sep + 'diagram'
        gviz = graphviz.Digraph("Pipeline",
                                filename=_temp_file,
                                node_attr={'color': 'lightblue2', 'style': 'filled'},
                                format=file_extension.replace('.', ''))
        gviz.attr(rankdir='TB', size='7,14')
        items = list(self)
        for _idx_1 in list(range(len(self))):
            _current_list = []
            _assigned = False
            for _idx_2 in list(range(len(self))):
                if _idx_1 == _idx_2:
                    continue
                if items[_idx_1].process is not None and items[_idx_2].process is not None:
                    if items[_idx_1].process == items[_idx_2].process.input_process:
                        _current_list.append(items[_idx_2].name)
                        gviz.edge(items[_idx_1].name, items[_idx_2].name)
                        _assigned = True
            if not _assigned:
                gviz.node(items[_idx_1].name)
        gviz.render(_file)
        # use numpy to construct an array from the bytes
        gviz.save(_temp_file)
        (graph,) = pydot.graph_from_dot_file(_temp_file)
        graph.write_png(_temp_file + '.png', prog=['dot', '-Gdpi={:}'.format(dpi)])
        fig_out = None

        if return_figure:
            img = pil.Image.open(_temp_file + '.png')
            fig_out = plt.figure()
            ax = fig_out.add_subplot(111)
            ax.imshow(img, aspect=1)
            ax.axis('off')
            fig_out.tight_layout()
        return fig_out


class ReSampling(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 new_sampling_rate: u.quantity.Quantity = None,
                 **kwargs):
        """
        This InputOutputProcess class will resample the data to the new_sampling_rate
        :param input_process: InputOutputProcess Class
        :param new_sampling_rate: float indicating the new sampling rate
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(ReSampling, self).__init__(input_process=input_process, **kwargs)
        self.new_sampling_rate = set_default_unit(new_sampling_rate, u.Hz)

    def transform_data(self):
        data, _factor = eegf.eeg_resampling(x=self.input_node.data,
                                            factor=self.new_sampling_rate / self.input_node.fs)
        self.output_node.data = data
        self.output_node.fs = self.input_node.fs * _factor
        self.output_node.process_history.append(self.process_parameters)


class ReferenceData(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 reference_channels: [str] = None,
                 remove_reference=True,
                 invert_polarity=False,
                 **kwargs):
        """
        This class will reference the data by subtracting the reference_channels (mean) from each individual channel
        :param input_process: InputOutputProcess Class
        :param reference_channels: list of string with the reference channels labels, if empty across channel average
        will be used
        :param remove_reference: boolean indicating whether to keep or remove the reference channel from the data
        :param invert_polarity: boolean indicating if channels polarity will be polarity inverted (data * -1)
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(ReferenceData, self).__init__(input_process=input_process, **kwargs)
        if reference_channels is None:
            reference_channels = ['']
        self.reference_channels = reference_channels
        self.remove_reference = remove_reference
        self.invert_polarity = invert_polarity

    def transform_data(self):
        _ch_idx = self.input_node.get_channel_idx_by_label(self.reference_channels)
        if not _ch_idx.size:
            self.remove_reference = False
            _ch_idx = np.arange(self.input_node.layout.size)
        print('Referencing data to: ' + ''.join(['{:s} '.format(_ch.label) for _ch in self.input_node.layout[_ch_idx]]))
        reference = np.mean(self.input_node.data[:, _ch_idx], axis=1, keepdims=True)
        self.output_node.data = (self.input_node.data - reference) * (-1.0) ** self.invert_polarity

        if self.remove_reference:
            self.output_node.delete_channel_by_idx(_ch_idx)


class FilterData(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 high_pass: u.quantity.Quantity = None,
                 low_pass: u.quantity.Quantity = None,
                 **kwargs):
        """
        Filter EEG data using a zero group-delay technique
        :param input_process: InputOutputProcess Class
        :param high_pass: Frequency (in Hz) of high-pass filter
        :param low_pass: Frequency (in Hz) of low-pass filter
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(FilterData, self).__init__(input_process=input_process, **kwargs)
        self.low_pass = set_default_unit(low_pass, u.Hz)
        self.high_pass = set_default_unit(high_pass, u.Hz)

    def transform_data(self):
        if self.low_pass is None and self.high_pass is None:
            self.output_node.data = self.input_node.data
            return
        filtered_signal = self.input_node.data.copy()
        _b = eegf.bandpass_fir_win(high_pass=self.high_pass, low_pass=self.low_pass, fs=self.input_node.fs)
        _b = _b * u.dimensionless_unscaled
        filtered_signal = filt_filt_data(input_data=filtered_signal, b=_b)
        self.output_node.data = filtered_signal


class HilbertEnvelope(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, high_pass=None, low_pass=None, **kwargs):
        """
        This process computes the  Hilbert Envelope of EEG data
        :param input_process: InputOutputProcess Class
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(HilbertEnvelope, self).__init__(input_process=input_process, **kwargs)

    def transform_data(self):
        data = self.input_node.data.copy()
        _fft = pyfftw.builders.fft(data, overwrite_input=False, planner_effort='FFTW_ESTIMATE', axis=0,
                                   threads=multiprocessing.cpu_count())
        fx = _fft()
        n = fx.shape[0]
        h = np.zeros(n)
        if n % 2 == 0:
            h[0] = h[n // 2] = 1
            h[1:n // 2] = 2
        else:
            h[0] = 1
            h[1:(n + 1) // 2] = 2
        _ifft = pyfftw.builders.ifft(fx * h.reshape(-1, 1), overwrite_input=False, planner_effort='FFTW_ESTIMATE',
                                     axis=0,
                                     threads=multiprocessing.cpu_count())
        hilbert_data = _ifft()
        self.output_node.data = np.abs(hilbert_data)


class RegressOutEOG(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 ref_channel_labels: [str] = None,
                 method: str = 'template',
                 high_pass: u.quantity.Quantity = 0.1 * u.Hz,
                 low_pass: u.quantity.Quantity = 20.0 * u.Hz,
                 peak_width: u.quantity.Quantity = 0.1 * u.s,
                 template_width: u.quantity.Quantity = 0.6 * u.s,
                 remove_eog_channels: bool = True,
                 save_figure: bool = True,  # save figure
                 fig_format: str = '.png',
                 user_naming_rule: str = '',
                 return_figures: bool = False,
                 **kwargs):
        """
        This class removes EOG artifacts using a template technique. Blinking artifacts are detected and averaged to
        generate a template. This template is scaled for each channel in order to maximize correlation between each
        individual blink and individual events on each channel
        The resulting template is removed from the data, reducing blinks artifacts.
        :param input_process: InputOutputProcess Class
        :param ref_channel_labels: a list with the channel labels that contain the EOG
        :param high_pass: Frequency (in Hz) of high-pass filter all data. This is necessary to generate the template
        :param low_pass: Frequency (in Hz) of low-pass filter only applied to EOG channels
        :param peak_width: default minimum width (in seconds) to detect peaks
        :param template_width: the duration (in seconds) of the time window to average and generate a template
        :param remove_eog_channels: if true EOG channels will be removed once data has been cleaned
        :param save_figure: whether to save or not figures showing the detection and removal of blinks
        :param fig_format: format of output figure
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(RegressOutEOG, self).__init__(input_process=input_process, **kwargs)
        self.ref_channel_labels = ref_channel_labels
        self.low_pass = low_pass
        self.high_pass = high_pass
        self.peak_width = peak_width
        self.template_width = template_width
        self.remove_eog_channels = remove_eog_channels
        self.save_figure = save_figure
        self.fig_format = fig_format
        self.method = method
        self.user_naming_rule = user_naming_rule
        self.return_figures = return_figures

    def transform_data(self):
        data = self.input_node.data.copy()
        _ref_idx = self.input_node.get_channel_idx_by_label(labels=self.ref_channel_labels)
        figure_dir_path = self.input_node.paths.figures_current_dir
        figure_basename = self.__class__.__name__ + '_eog_' + self.user_naming_rule
        artefact_method = self.method
        if artefact_method is None:
            if data.ndim == 3:
                artefact_method = 'correlation'
            else:
                artefact_method = 'template'

        if _ref_idx.size:
            print('Using {:} to remove eog artifacts'.format(artefact_method))
            if artefact_method == 'template':
                data, fig = et_subtract_oeg_template(data=data,
                                                     idx_ref=np.array(_ref_idx),
                                                     high_pass=self.high_pass,
                                                     low_pass=self.low_pass,
                                                     fs=self.input_node.fs,
                                                     template_width=self.template_width,
                                                     plot_results=self.save_figure,
                                                     figure_path=figure_dir_path,
                                                     figure_basename=figure_basename)
            if artefact_method == 'correlation':
                data, fig = et_subtract_correlated_ref(data=data,
                                                       idx_ref=np.array(_ref_idx),
                                                       high_pass=self.high_pass,
                                                       low_pass=self.low_pass,
                                                       fs=self.input_node.fs,
                                                       plot_results=self.save_figure,
                                                       figure_path=figure_dir_path,
                                                       figure_basename=figure_basename)

            if self.return_figures:
                self.figures = fig
            else:
                plt.close(fig)
        else:
            print('Reference channels for eye artefact reduction are not valid! Will return data unaltered.')

        self.output_node.data = data
        if self.remove_eog_channels and _ref_idx.size:
            self.output_node.delete_channel_by_idx(_ref_idx)


class RegressOutICA(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 ref_channel_labels: [str] = None,
                 **kwargs):
        """
        This class uses independent component analysis to remove EOG activity. The removal is based on the correlation
        between the ICA and the reference channels (containing large EOG activity)
        :param input_process: InputOutputProcess Class
        :param ref_channel_labels: a list with the channel labels that contain the EOG
        :param kwargs:
        """
        super(RegressOutICA, self).__init__(input_process=input_process, **kwargs)
        self.ref_channel_labels = ref_channel_labels

    def transform_data(self):
        data = self.input_node.data.copy()
        _ref_idx = self.input_node.get_channel_idx_by_label(labels=self.ref_channel_labels)
        _cov = data[:, _ref_idx]
        _offset = int(data.shape[0] * 0.10)

        # Compute ICA
        print('Performing ICA analysis')
        ica = FastICA(n_components=data.shape[1], max_iter=1000)
        ica.fit(data)
        # decompose signal into components
        components = ica.fit_transform(data)
        corr_coefs = np.empty((components.shape[1], _cov.shape[1]))
        for _i_com, _i_cov in itertools.product(range(components.shape[1]), range(_cov.shape[1])):
            corr_coefs[_i_com, _i_cov] = \
                np.corrcoef(_cov[_offset:-_offset, _i_cov], components[_offset:-_offset, _i_com])[0, 1]
        _idx_to_remove = np.argmax(np.abs(corr_coefs), axis=0)
        print('Maximum correlations: {:}'.format(corr_coefs[np.argmax(np.abs(corr_coefs), axis=0), :]))
        print('Removing components: {:}'.format(_idx_to_remove))
        components[:, _idx_to_remove] = 0
        clean_data = ica.inverse_transform(components)
        self.output_node.data = clean_data


class RegressOutCCA(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, ref_channel_labels: [str] = None, **kwargs):
        super(RegressOutCCA, self).__init__(input_process=input_process, **kwargs)
        self.channel_labels = ref_channel_labels

    def transform_data(self):
        data = self.input_node.data.copy()
        _ref_idx = self.input_node.get_channel_idx_by_label(labels=self.ref_channel_labels)
        _cov = data[:, _ref_idx]
        # Compute ICA
        print('Performing ICA analysis')

        cca = CCA(n_components=data.shape[1])
        cca.fit(data, _cov)
        # decompose signal into components
        X_c, Y_c = cca.transform(data, _cov)
        # corr_coefs = np.empty((components.shape[1], _cov.shape[1]))
        # for _i_com, _i_cov in itertools.product(range(components.shape[1]), range(_cov.shape[1])):
        #     corr_coefs[_i_com, _i_cov] = \
        #     np.corrcoef(_cov[_offset:-_offset, _i_cov], components[_offset:-_offset, _i_com])[0, 1]
        # _idx_to_remove = np.argmax(np.abs(corr_coefs), axis=0)
        # print('Maximum correlations: {:}'.format(corr_coefs[np.argmax(np.abs(corr_coefs), axis=0), :]))
        # print('Removing components: {:}'.format(_idx_to_remove))
        # components[:, _idx_to_remove] = 0
        # clean_data = ica.inverse_transform(components)

        # self.output_node.data = clean_data


class EpochData(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 pre_stimulus_interval: u.quantity.Quantity = None,
                 post_stimulus_interval: u.quantity.Quantity = None,
                 event_code: float = None,
                 base_line_correction: bool = False,
                 de_trend: bool = True,
                 **kwargs):
        """
        This class will take a n*m matrix into a k*m*p, where p (number of trials) is determined by the number of
        triggers used to split the data
        :param input_process: InputOutputProcess Class
        :param pre_stimulus_interval: the length (in sec) of the data to be read before the trigger
        :param post_stimulus_interval: the length (in sec) of the data to be read after the trigger
        :param event_code: integer indicating the event code to be used to epoch the data
        :param base_line_correction: whether to perform beaseline correction or not, the timespan to calculate the mean
        for baseline correction from, can be defined by setting pre_stimulus_interval, e.g. 200 would perform
        the baseline correction from -200 to 0 seconds before trigger onset.
        :param de_trend: whether to remove linear components from each epoch, subtracts complete epoch mean from epoch
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(EpochData, self).__init__(input_process=input_process, **kwargs)
        self.pre_stimulus_interval = set_default_unit(pre_stimulus_interval, u.s)
        self.post_stimulus_interval = set_default_unit(post_stimulus_interval, u.s)
        self.event_code = event_code
        self.base_line_correction = base_line_correction
        self.de_trend = de_trend

    def transform_data(self):
        if self.event_code is None:
            print('no event code was provided')
            return
        data = self.input_node.data
        pre_stimulus_interval = 0.0 * u.s if self.pre_stimulus_interval is None else self.pre_stimulus_interval
        events_index = self.input_node.events.get_events_index(code=self.event_code, fs=self.input_node.fs)
        post_stimulus_interval = self.post_stimulus_interval
        if self.post_stimulus_interval is None:
            post_stimulus_interval = np.percentile(np.diff(events_index), 90) / self.input_node.fs
        buffer_size = np.int((post_stimulus_interval + pre_stimulus_interval) * self.input_node.fs)
        events_index = events_index - int((pre_stimulus_interval + self.input_node.x_offset) * self.input_node.fs)
        events_index = events_index[events_index >= 0]
        epochs = np.zeros((buffer_size, self.input_node.data.shape[1], events_index.size), dtype=np.float32)
        for i, _event in enumerate(events_index):
            # ensure that blocks match buffer size
            if _event + buffer_size > self.input_node.data.shape[0]:
                epochs = epochs[:, :, list(range(i))]
                break
            epochs[:, :, i] = data[_event:_event + buffer_size, :]
        epochs = epochs * data.unit
        print('A total of {:} epochs were obtained using event code {:}, each with a duration of {:.3f}'.format(
            epochs.shape[2],
            self.event_code,
            (buffer_size / self.input_node.fs).to(u.s)))
        if self.de_trend:
            epochs -= np.mean(epochs, axis=0)
        if self.base_line_correction:
            _ini_sample = 0
            _end_sample = max(1, int(pre_stimulus_interval * self.input_node.fs))
            epochs -= np.mean(epochs[_ini_sample:_end_sample, :, :], axis=0)
        self.output_node.data = epochs
        self.output_node.x_offset = pre_stimulus_interval
        self.output_node.events = None


class RejectEpochs(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 std_threshold: float = 0.0,
                 rejection_percentage: float = 0.0,
                 rejection_threshold: u.quantity.Quantity = 100.0 * u.uV,
                 max_percentage_epochs_above_threshold: float = 1.0,
                 **kwargs):
        """
        This class will epochs where a given threshold have been exceeded. If any channel exceed the threshold at a
        given trial, that particular epoch will be removed from all channels. This is done to keep the data in a single
        matrix.
        :param input_process: InputOutputProcess Class
        :param std_threshold: float indicating the threshold in terms of standard deviations to remove epochs
        :param max_percentage_epochs_above_threshold: if a single channel has more than max_epochs_above_threshold
        (percentage) epochs, the channel will be removed.
        :param rejection_percentage indicates the percentage of epochs to remove.
        above the threshold, the channel will be removed.
        :param rejection_threshold: indicates level above which epochs will be rejected
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(RejectEpochs, self).__init__(input_process=input_process, **kwargs)
        self.std_threshold = set_default_unit(std_threshold, u.dimensionless_unscaled)
        self.rejection_threshold = set_default_unit(rejection_threshold, u.uV)
        self.max_percentage_epochs_above_threshold = max_percentage_epochs_above_threshold
        self.rejection_percentage = rejection_percentage

    def transform_data(self):
        _data = self.input_node.data
        _idx_to_keep = np.ones((_data.shape[2], ), dtype=bool)
        # first we check if a channel needs to be removed  based on max_percentage_epochs_above_threshold
        if self.max_percentage_epochs_above_threshold < 1.0:
            _max_amp = np.max(np.abs(_data), axis=0)
            _epoch_limit = int(self.max_percentage_epochs_above_threshold * _data.shape[2])
            _n_noisy_epochs_per_channel = np.sum(_max_amp > self.rejection_threshold, axis=1)
            _ch_to_remove = np.argwhere(_n_noisy_epochs_per_channel > _epoch_limit).flatten()
            print('Removing a total of {:} channels with more than {:} epochs above the threshold {:}'.format(
                np.sum(_ch_to_remove.size), _epoch_limit, self.rejection_threshold))
            # we assign data to output node to perform a clean delete of bad channels
            self.output_node.data = _data
            self.output_node.delete_channel_by_idx(_ch_to_remove)
            _data = self.output_node.data

        # now using the new data we find and remove the x % of ephocs with the highest peak
        if self.rejection_percentage > 0:
            _max_amp = np.max(np.abs(_data), axis=0)
            rejection_thr = np.quantile(np.max(_max_amp, axis=0), 1 - self.rejection_percentage)
            to_keep = np.max(_max_amp, axis=0, keepdims=True) < rejection_thr
            _idx_to_keep = np.logical_and(_idx_to_keep, np.all(to_keep, axis=0).flatten())

        if self.rejection_threshold > 0:
            # now we find epochs indexes across channels which are avove threshold
            _max_amp = np.max(np.abs(_data), axis=0)
            to_keep = _max_amp <= self.rejection_threshold
            _idx_to_keep = np.logical_and(_idx_to_keep, np.all(to_keep, axis=0).flatten())

        if self.std_threshold > 0:
            _epochs_std = np.std(_data, axis=0)
            _n_noisy_epochs_across_channels = np.sum(np.greater(_epochs_std,
                                                                (np.mean(_epochs_std, 1) +
                                                                 self.std_threshold * np.std(_epochs_std, 1)).reshape(
                                                                    -1, 1)),
                                                     axis=0,
                                                     keepdims=True)
            to_keep = _n_noisy_epochs_across_channels == 0
            _n_std_to_remove = np.sum(~to_keep)
            _std_percentage = 100 * _n_std_to_remove / _data.shape[2]
            _idx_to_keep = np.logical_and(_idx_to_keep, np.all(to_keep, axis=0).flatten())
            print('Rejecting a total of {:} epochs, corresponding to {:} % above {:} std'.format(
                np.sum(~_idx_to_keep), _std_percentage, self.std_threshold))
        # print some information and reject epochs
        _p_rejected = 100 * (1 - np.sum(_idx_to_keep) / _data.shape[2])
        print('Percentage of total epochs rejected {:}%'.format(_p_rejected))
        if _p_rejected == 100:
            raise Exception('All epochs were rejected!! check rejection level '
                            '(currently {:}) and run again'.format(self.rejection_percentage))

        self.output_node.data = _data[:, :, _idx_to_keep.flatten()]


class RemoveBadChannelsEpochsBased(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 threshold: float = 300.0,
                 max_epochs_above_threshold: float = 0.5,
                 **kwargs):
        """
        This class will remove a channel based on epochs data. If any channel has more than max_epochs_above_threshold
        percentage of epochs above a given threshold, this channel will be removed.
        :param input_process: InputOutputProcess Class
        :param threshold: the threshold to reject channels with too much noise
        :param max_epochs_above_threshold: if a single channel has more than max_epochs_above_threshold (percentage)
        epochs, the channel will be removed.
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(RemoveBadChannelsEpochsBased, self).__init__(input_process=input_process, **kwargs)
        self.threshold = threshold
        self.max_epoch_above_threshold = max_epochs_above_threshold

    def transform_data(self):
        _data = self.input_node.data
        _max_amp = np.max(np.abs(_data), axis=0)
        _epoch_limit = int(self.max_epoch_above_threshold * _data.shape[2])
        _n_noisy_epochs_per_channel = np.sum(_max_amp > self.threshold, axis=1)
        _ch_to_remove = np.argwhere(_n_noisy_epochs_per_channel > _epoch_limit).flatten()
        print('Removing a total of {:} channels with more than {:} epochs above the threshold {:}'.format(
            np.sum(_ch_to_remove.size), _epoch_limit, self.threshold))
        # we assign data to output node to perform a clean delete of bad channels
        self.output_node.data = _data
        self.output_node.delete_channel_by_idx(_ch_to_remove)


class SortEpochs(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 ascending=False,
                 sort_by: str = 'std',
                 **kwargs):
        """
        This class will sort epochs by maximum amplitude across all trials.
        :param input_process: InputOutputProcess Class
        :param sort_by: string indicating the method to sort ephocs, either 'std' for standard deviation or 'amp' to use
        maximum amplitude per epoch.
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(SortEpochs, self).__init__(input_process=input_process, **kwargs)
        self.ascending = ascending
        self.sort_by = sort_by

    def transform_data(self):
        _data = self.input_node.data
        if self.sort_by == 'amp':
            _measured_value = np.max(np.abs(_data), axis=0)
            _idx_sorted = np.argsort(_measured_value, axis=1)
        if self.sort_by == 'std':
            _measured_value = np.std(_data, axis=0)
            _idx_sorted = np.argsort(_measured_value, axis=1)
        sorted_data = np.zeros(_data.shape) * _data.unit
        if self.ascending:
            for _ch in range(_idx_sorted.shape[0]):
                sorted_data[:, _ch, :] = _data[:, _ch, _idx_sorted[_ch, :][::-1]]
        else:
            for _ch in range(_idx_sorted.shape[0]):
                sorted_data[:, _ch, :] = _data[:, _ch, _idx_sorted[_ch, :]]

        print('Sorting epochs by {:}'.format(self.sort_by))
        self.output_node.data = sorted_data


class SpatialFilter(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 sf_join_frequencies: np.array = None,
                 demean_data: bool = True,
                 weight_data: bool = True,
                 keep0: int = None,
                 keep1: float = 1e-9,
                 perc0: float = .99,
                 perc1: float = None,
                 block_size: int = 10,
                 delta_frequency: u.Quantity = 5 * u.Hz,
                 **kwargs):
        """
        This class will create an spatial filter based on the data. If sf_join_frequencies are passed, the spatial
        filter will use a biased function based on the covariance of those frequencies only.
        :param input_process:  InputOutputProcess Class
        :param sf_join_frequencies: numpy array indicating the biased frequencies (useful for steady-state responses)
        :param demean_data: if true, data will be demeaned prior filter estimation
        :param weight_data: if true, dss filter will be estimated using weights
        :param keep0: integer controlling  whitening of unbiased components in DSS. This integer value represent the
        number of components to keep.
        :param keep1: float controlling  whitening of unbiased components in DSS. This value will remove components
        below keep1 which is relative to the maximum eigen value.
        :param perc0: float (between 0 and 1) controlling whitening of unbiased components in DSS.
        This value will preserve components that explain the percentage of variance.
        :param perc1: float (between 0 and 1) controlling the number of biased components kept in DSS.
        This value will preserve the components of the biased PCA analysis that explain the percentage of variance.
        :param block_size: integer indicating the number of trials that would be use to estimate the weights
        :param delta_frequency: frequency size around each sf_join_frequency to estimate noise
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(SpatialFilter, self).__init__(input_process=input_process, **kwargs)
        self.sf_join_frequencies = set_default_unit(sf_join_frequencies, u.Hz)
        self.demean_data = demean_data
        self.weight_data = weight_data
        self.pwr0 = None
        self.pwr1 = None
        self.cov = None
        self.keep0 = keep0
        self.keep1 = keep1
        self.perc0 = perc0
        self.perc1 = perc1
        self.block_size = block_size
        self.delta_frequency = delta_frequency

    def transform_data(self):
        # compute spatial filter
        z, pwr0, pwr1, cov,  _ = et_get_spatial_filtering(epochs=self.input_node.data,
                                                          fs=self.input_node.fs,
                                                          sf_join_frequencies=self.sf_join_frequencies,
                                                          demean_data=self.demean_data,
                                                          weight_data=self.weight_data,
                                                          weights=self.input_node.w,
                                                          keep0=self.keep0,
                                                          keep1=self.keep1,
                                                          perc0=self.perc0,
                                                          perc1=self.perc1,
                                                          block_size=self.block_size,
                                                          delta_frequency=self.delta_frequency)
        self.output_node.data = z * u.dimensionless_unscaled
        self.pwr0 = pwr0
        self.pwr1 = pwr1
        self.cov = cov


class ApplySpatialFilter(InputOutputProcess):
    def __init__(self,
                 input_process: SpatialFilter = None,
                 sf_components: np.array = np.array([]),
                 sf_thr: float = 0.8,
                 **kwargs):
        """
        This class will apply an SpatialFilter to the data. If sf_components are paased, only those will be used to
        filter the data
        :param input_process: an SpatialFilter InputOutputProcess Class
        :param sf_components: numpy array of integers with indexes of components to be kept
        :param sf_thr: float indicating the percentage of explained variance by components to be kept (this is used when
        sf_components is empty)
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(ApplySpatialFilter, self).__init__(input_process=input_process, **kwargs)
        self.sf_components = sf_components
        self.sf_thr = sf_thr

    def transform_data(self):
        filtered_data, _ = et_apply_spatial_filtering(z=self.input_node.data,
                                                      pwr0=self.input_process.pwr0,
                                                      pwr1=self.input_process.pwr1,
                                                      cov_1=self.input_process.cov,
                                                      sf_components=self.sf_components,
                                                      sf_thr=self.sf_thr)
        self.output_node.data = filtered_data


class CreateAndApplySpatialFilter(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 sf_join_frequencies: np.array = None,
                 sf_components=np.array([]),
                 sf_thr: float = 0.8,
                 keep0: int = None,
                 keep1: float = 1e-9,
                 perc0: float = .99,
                 perc1: float = None,
                 delta_frequency: u.Quantity = 5 * u.Hz,
                 block_size: int = 10,
                 demean_data: bool = True,
                 weight_data: bool = True,
                 projection_domain: Domain = Domain.time,
                 components_to_plot: np.array = np.arange(0, 10),
                 plot_x_lim: [float, float] = None,
                 plot_y_lim: [float, float] = None,
                 user_naming_rule: str = '',
                 fig_format: str = '.png',
                 return_figures: bool = False,
                 save_to_file: bool = True,
                 **kwargs):
        """
        This class will create and apply an spatial filter to the data.
        :param input_process: an InputOutputProcess Class
        :param sf_join_frequencies: numpy array indicating the biased frequencies (useful for steady-state responses)
        :param sf_components: numpy array of integers with indexes of components to be kept
        :param sf_thr: float indicating the percentage of explained variance by components to be kept (this is used when
        sf_components is empty)
        :param keep1: float controlling  whitening of unbiased components in DSS. This value will remove components
        below keep1 which is relative to the maximum eigen value.
        :param perc0: float (between 0 and 1) controlling whitening of unbiased components in DSS.
        This value will preserve components that explain the percentage of variance.
        :param perc1: float (between 0 and 1) controlling the number of biased components kept in DSS.
        This value will preserve the components of the biased PCA analysis that explain the percentage of variance.
        :param delta_frequency: frequency size around each sf_join_frequency to estimate noise
        :param demean_data: If true, filter will be created with demeaned data
        :param weight_data: if true, weighted average will be use to create the filter
        :param projection_domain: indicate the domain in which component will be projected in plots
        :param components_to_plot: numpy array indicating which components will be plotted.
        :param plot_x_lim: list with minimum and maximum horizontal range of x axis
        :param plot_y_lim: list with minimum and maximum vertical range of y axis
        :param user_naming_rule: string indicating a user naming to be included in the figure file name
        :param fig_format: string indicating the format of the output figure (e.g. '.png' or '.pdf')
        :param return_figures: bool indicating if figure handle should be returned in self.figures
        :param save_to_file: bool indicating whether figure should be saved to file
        :param block_size: number of trials that will be stack together to estimate the residual noise
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(CreateAndApplySpatialFilter, self).__init__(input_process=input_process, **kwargs)
        self.sf_join_frequencies = set_default_unit(sf_join_frequencies, u.Hz)
        self.sf_components = sf_components
        self.sf_thr = sf_thr
        self.keep0 = keep0
        self.keep1 = keep1
        self.perc0 = perc0
        self.perc1 = perc1
        self.delta_frequency = delta_frequency
        self.block_size = block_size
        self.components_to_plot = components_to_plot
        self.projection_domain = projection_domain
        self.demean_data = demean_data
        self.weight_data = weight_data
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.user_naming_rule = user_naming_rule
        self.fig_format = fig_format
        self.return_figures = return_figures
        self.save_to_file = save_to_file
        self.__kwargs = kwargs

    def transform_data(self):
        figures = None
        if self.return_figures:
            figures = []
        spatial_filter = SpatialFilter(input_process=self.input_process,
                                       sf_join_frequencies=self.sf_join_frequencies,
                                       demean_data=self.demean_data,
                                       weight_data=self.weight_data,
                                       keep0=self.keep0,
                                       keep1=self.keep1,
                                       perc0=self.perc0,
                                       perc1=self.perc1,
                                       block_size=self.block_size,
                                       delta_frequency=self.delta_frequency
                                       )
        spatial_filter.run()
        filtered_data = ApplySpatialFilter(input_process=spatial_filter,
                                           sf_components=self.sf_components,
                                           sf_thr=self.sf_thr,
                                           **self.__kwargs)
        filtered_data.run()
        self.output_node = filtered_data.output_node
        if self.components_to_plot is not None:
            plotter_1 = PlotSpatialFilterComponents(spatial_filter,
                                                    plot_x_lim=self.plot_x_lim,
                                                    plot_y_lim=self.plot_y_lim,
                                                    user_naming_rule=self.user_naming_rule,
                                                    fig_format=self.fig_format,
                                                    components_to_plot=self.components_to_plot,
                                                    domain=self.projection_domain,
                                                    return_figures=self.return_figures,
                                                    save_to_file=self.save_to_file
                                                    )
            plotter_1.run()
            if self.return_figures:
                figures.append(plotter_1.figures)
            if self.save_to_file:
                plotter_2 = ProjectSpatialComponents(spatial_filter,
                                                     user_naming_rule=self.user_naming_rule,
                                                     plot_x_lim=self.plot_x_lim,
                                                     plot_y_lim=self.plot_y_lim,
                                                     components_to_plot=self.components_to_plot,
                                                     domain=self.projection_domain,
                                                     return_figures=self.return_figures
                                                     )
                plotter_2.run()
                if self.return_figures:
                    figures += plotter_2.figures
        self.figures = figures


class PlotSpatialFilterComponents(InputOutputProcess):
    def __init__(self, input_process=SpatialFilter,
                 components_to_plot: np.array = np.arange(0, 10),
                 domain: Domain = Domain.time,
                 user_naming_rule: str = 'components',
                 plot_x_lim: [float, float] = None,
                 plot_y_lim: [float, float] = None,
                 fig_format: str = '.png',
                 return_figures: bool = False,
                 save_to_file: bool = True,
                 **kwargs):
        """
        This class plots the components waveforms (from an SpatialFilter)
        :param input_process: anInputOutputProcess Class
        :param components_to_plot: numpy array indicating which components will be plotted.
        :param domain: indicate the domain in which component will be projected
        :param user_naming_rule: string indicating a user naming to be included in the figure file name
        :param plot_x_lim: list with minimum and maximum horizontal range of x axis
        :param plot_y_lim: list with minimum and maximum vertical range of y axis
        :param fig_format: string indicating the format of the output figure (e.g. '.png' or '.pdf')
        :param return_figures: bool indicating if figure should be returned in self.figures
        :param save_to_file: bool indicating whether figure should be saved to file
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(PlotSpatialFilterComponents, self).__init__(input_process=input_process, **kwargs)
        self.components_to_plot = components_to_plot
        self.domain = domain
        self.user_naming_rule = user_naming_rule
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.fig_format = fig_format
        self.return_figures = return_figures
        self.save_to_file = save_to_file
        self.__kwargs = kwargs

    def transform_data(self):
        if self.components_to_plot is not None:
            # we copy the input process to overwrite the layout without affecting other pipe processes
            _input_process = copy.deepcopy(self.input_process)

            # _input_process.output_node.y_units = u.def_unit('A.U.')
            # generate a generic layout
            _input_process.output_node.layout = np.array([ChannelItem()
                                                          for _ in range(_input_process.output_node.data.shape[1])])
            for _i, _lay in enumerate(_input_process.output_node.layout):
                _lay.label = str(_i)

            if self.domain == Domain.time:
                average = AverageEpochs(_input_process)

            if self.domain == Domain.frequency:
                average = AverageEpochsFrequencyDomain(_input_process,
                                                       test_frequencies=self.input_process.sf_join_frequencies,
                                                       delta_frequency=self.input_process.delta_frequency,
                                                       weight_frequencies=self.input_process.sf_join_frequencies
                                                       )

            average.run()
            plotter = PlotWaveforms(average,
                                    user_naming_rule='_{:}_{:}_components'.format(
                                        self.user_naming_rule,
                                        average.output_node.data.shape[1]),
                                    ch_to_plot=self.components_to_plot,
                                    plot_x_lim=self.plot_x_lim,
                                    plot_y_lim=None,
                                    fig_format=self.fig_format,
                                    return_figures=self.return_figures,
                                    save_to_file=self.save_to_file)
            plotter.run()
            self.figures = plotter.figures


class ProjectSpatialComponents(InputOutputProcess):
    def __init__(self, input_process=SpatialFilter,
                 components_to_plot: np.array = np.arange(0, 10),
                 user_naming_rule: str = '',
                 plot_x_lim: [float, float] = None,
                 plot_y_lim: [float, float] = None,
                 domain: Domain = Domain.time,
                 return_figures: bool = False,
                 **kwargs):
        """
        This class will project back to the sensor space each component and plot them in a topographic map.
        :param input_process: an SpatialFilter InputOutputProcess Class
        :param components_to_plot: numpy array indicating which components will be plotted.
        :param user_naming_rule: string indicating a user naming to be included in the figure file name
        :param plot_x_lim: list with minimum and maximum horizontal range of x axis
        :param plot_y_lim: list with minimum and maximum vertical range of y axis
        :param domain: indicate the domain in which component will be projected
        :param return_figures: if true, figures will be returned in self.figures
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(ProjectSpatialComponents, self).__init__(input_process=input_process, **kwargs)
        self.components_to_plot = components_to_plot
        self.user_naming_rule = user_naming_rule
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.domain = domain
        self.return_figures = return_figures
        self.__kwargs = kwargs

    def transform_data(self):
        figures = []
        for _component in self.components_to_plot:
            if _component > self.input_process.output_node.data.shape[1] - 1:
                continue
            if isinstance(self.input_process, SpatialFilter):
                projected_component = ApplySpatialFilter(input_process=self.input_process,
                                                         sf_components=np.array([_component]))
            if isinstance(self.input_process, SpatialFilterICA):
                projected_component = ApplySpatialFilterICA(input_process=self.input_process,
                                                            sf_components=np.array([_component]))
            projected_component.run()
            if self.domain == Domain.time:
                average = AverageEpochs(projected_component)
            if self.domain == Domain.frequency:
                average = AverageEpochsFrequencyDomain(projected_component,
                                                       test_frequencies=self.input_process.sf_join_frequencies,
                                                       weight_frequencies=self.input_process.sf_join_frequencies,
                                                       delta_frequency=self.input_process.delta_frequency)
            average.run()
            times = None
            if self.domain == Domain.time:
                max_chan = np.argmax(np.std(average.output_node.data, axis=0))
                cha_max_pow_label = average.output_node.layout[max_chan].label
                # find max and min within plot_x_lim
                if self.plot_x_lim is not None:
                    self.plot_x_lim = set_default_unit(self.plot_x_lim, average.output_node.x.unit)
                    _samples = average.output_node.x_to_samples(self.plot_x_lim)
                    _max = np.argmax(average.output_node.data[_samples[0]: _samples[-1], max_chan]) + _samples[0]
                    _min = np.argmin(average.output_node.data[_samples[0]: _samples[-1], max_chan]) + _samples[0]
                else:
                    _max = np.argmax(average.output_node.data[:, max_chan])
                    _min = np.argmin(average.output_node.data[:, max_chan])
                times = np.sort(np.array(average.output_node.x[np.array([_max, _min])]))
            if self.domain == Domain.frequency:
                if self.input_process.sf_join_frequencies is not None:
                    _idx_f = average.output_node.x_to_samples(self.input_process.sf_join_frequencies)
                    max_chan = np.argmax(np.max(np.abs(average.output_node.data[_idx_f, :]), axis=0))
                    cha_max_pow_label = average.output_node.layout[max_chan].label

            # plot time potential fields for average channel
            plotter = PlotTopographicMap(average,
                                         user_naming_rule=self.user_naming_rule + '_component_{:}'.format(_component),
                                         topographic_channels=np.array([cha_max_pow_label]),
                                         plot_x_lim=self.plot_x_lim,
                                         plot_y_lim=self.plot_y_lim,
                                         return_figures=self.return_figures,
                                         title='Component {:}'.format(_component),
                                         times=times)
            plotter.run()
            figures += plotter.figures
        self.figures = figures


class AutoRemoveBadChannels(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 thr_sd=5.0 * u.dimensionless_unscaled,
                 amp_thr=50000.0 * u.uV,
                 interval=0.001 * u.s,
                 **kwargs):
        """
        This function will try to detect bad channels by looking at the standard deviation across channels.
        It will remove any channels with and std largerd than thr_sd, which is computed across all channels.
        It also remove any channel which amplitude exceeds amp_thr.
        :param input_process: InputOutputProcess Class
        :param thr_sd: threshold standard deviation to remove channels. Channels with larger std will be removed
        :param amp_thr: threshold ampltitude. Channel exceeding this will be removed
        :param interval: time interval to subsample the data before estimating std (this is used to speed up the process
        in very long data files
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(AutoRemoveBadChannels, self).__init__(input_process=input_process, **kwargs)
        self.thr_sd = thr_sd
        self.amp_thr = set_default_unit(amp_thr, u.uV)
        self.interval = set_default_unit(interval, u.s)

    def transform_data(self):
        if self.input_node.data.ndim == 3:
            data = et_unfold(self.input_node.data)
        else:
            data = self.input_node.data
        step_size = np.maximum(int(self.interval * self.input_node.fs), 1)
        _samples = np.arange(0, self.input_node.data.shape[0], step_size).astype(np.int)
        sub_data = data[_samples, :]
        # compute dc component
        _dc_component = np.mean(np.abs(sub_data), axis=0)
        bad_channels = np.where(_dc_component > self.amp_thr)[0]
        _others_idx = np.array([idx for idx in np.arange(sub_data.shape[1]) if idx not in bad_channels], dtype=np.int)
        a_std = np.std(sub_data[:, _others_idx], axis=0)
        thr_ci = self.thr_sd * np.std(a_std) + np.mean(a_std)
        n_ch_idx = np.where(a_std > thr_ci)[0]
        bad_idx = _others_idx[n_ch_idx] if n_ch_idx.size else np.array([], dtype=np.int)
        bad_channels = np.concatenate((bad_channels, bad_idx))
        _bad_channels_index = np.unique(bad_channels)
        self.output_node.data = self.input_node.data.copy()
        self.output_node.delete_channel_by_idx(_bad_channels_index)


class RemoveBadChannels(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, bad_channels: [str] = None, **kwargs):
        """
        This class will remove any channel passed in bad_channel
        :param input_process: InputOutputProcess Class
        :param bad_channels: list of strings with the label of the channels to be removed
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(RemoveBadChannels, self).__init__(input_process=input_process, **kwargs)
        if bad_channels is None:
            bad_channels = ['']
        self.bad_channels = bad_channels

    def transform_data(self):
        idx_bad_channels = [_i for _i, _item in enumerate(self.input_node.layout) if _item.label in self.bad_channels]
        _bad_channels_index = np.unique(idx_bad_channels)
        self.output_node.data = self.input_node.data.copy()
        if _bad_channels_index.size:
            self.output_node.delete_channel_by_idx(_bad_channels_index)


class AverageEpochs(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 weighted_average: bool = True,
                 n_tracked_points: int = 256,
                 block_size: int = 5,
                 roi_windows: np.array([TimeROI]) = None,
                 **kwargs):
        """
        This InputOutputProcess average epochs
        :param input_process: InputOutputProcess Class
        :param weighted_average: if True, weighted average will be used
        :param n_tracked_points: number of equally spaced points over time used to estimate residual noise
        :param block_size: number of trials that will be stack together to estimate the residual noise
        :param roi_windows: time windows used to perform some measures (snr, rms)
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(AverageEpochs, self).__init__(input_process=input_process, **kwargs)
        self.weighted_average = weighted_average
        self.n_tracked_points = n_tracked_points
        self.block_size = block_size
        self.roi_windows = roi_windows

    def transform_data(self):
        block_size = max(self.block_size, 5)
        samples_distance = int(max(self.input_node.data.shape[0] // self.n_tracked_points, 10))
        w_ave, w, rn, cum_rn, w_fft, n = \
            et_mean(epochs=self.input_node.data,
                    block_size=block_size,
                    samples_distance=samples_distance,
                    weighted=self.weighted_average
                    )
        self.output_node.data = w_ave
        self.output_node.rn = rn
        self.output_node.cum_rn = cum_rn
        self.output_node.w = w
        self.output_node.n = n
        self.output_node.rn_df = block_size * int(self.input_node.data.shape[0] / samples_distance)
        self.output_node.statistical_tests = self.get_snr_table()
        self.output_node.roi_windows = self.roi_windows

    def get_snr_table(self):
        roi_samples = self.get_roi_samples()
        final_snr, _ = et_snr_in_rois(data=self.output_node.data,
                                      roi_windows=roi_samples,
                                      rn=self.output_node.rn)
        tests = []
        for _iw in range(len(roi_samples)):
            _ini = roi_samples[_iw][0]
            _end = roi_samples[_iw][1]
            # we estimate the degrees of freedom for signal from spectral domain.
            # the assumption is that a full white-noise will have the total signal length as degrees of freedom
            # if data is filtered, spectral content is not longer flat and degrees of freedom decrease
            _data_chunk = self.output_node.data[_ini: _end, :]
            _fft_power = np.abs(np.fft.rfft(_data_chunk, axis=0)) ** 2
            _ref_power = np.max(_fft_power, axis=0)
            df_signal = 2 * np.sum(_fft_power, axis=0) / _ref_power
            for _idx_ch, _ch in enumerate(self.output_node.layout):
                df_num = df_signal[_idx_ch]
                df_den = self.output_node.rn_df
                f_value = final_snr[_idx_ch, _iw] + 1
                tests.append(
                    FpmTest(df_1=df_num,
                            df_2=df_den,
                            f=f_value,
                            f_critic=f.ppf(1 - self.output_node.alpha, df_num, df_den),
                            p_value=1 - f.cdf(f_value, df_num, df_den),
                            rn=self.output_node.rn[_idx_ch],
                            snr=final_snr[_idx_ch, _iw],
                            time_ini=_ini / self.output_node.fs,
                            time_end=_end / self.output_node.fs,
                            n_epochs=self.output_node.n,
                            channel=_ch.label).__dict__
                )
        out = pd.DataFrame(tests)
        return out

    # def get_cumulative_snr_table(self):
    #     cumulative_snr = []
    #     n_trials = np.cumsum(self.output_node.nk).astype(int)
    #     roi_samples = self.get_roi_samples()
    #     tests = []
    #     for _b in range(self.output_node.cum_rn.shape[0]):
    #         c_data = self.input_node.data[_ini: _end, :, 0: n_trials[_b]]
    #         c_weights = self.output_node.w[:, 0: n_trials[_b]]
    #         cum_w_ave = np.sum(
    #             c_data * c_weights, axis=2)
    #         c_snr, _ = et_snr_in_rois(data=cum_w_ave,
    #                                   rn=self.output_node.cum_rn[_b, :],
    #                                   roi_windows=roi_samples)
    #         for _i in range(len(roi_samples)):
    #             _ini = roi_samples[_i][0]
    #             _end = roi_samples[_i][1]
    #             # we estimate the degrees of freedom for signal from spectral domain.
    #             # the assumption is that a full white-noise will have the total signal length as degrees of freedom
    #             # if data is filtered, spectral content is not longer flat and degrees of freedom decrease
    #             _fft = np.abs(np.fft.rfft(c_data, axis=0))
    #             _ref = np.max(_fft, axis=0)
    #             df_signal = _fft.shape[0] * np.sum(_fft, axis=0) / (_ref * _fft.shape[0])
    #             for _idx_ch, _ch in enumerate(self.output_node.layout):
    #                 df_num = df_signal[_idx_ch]
    #                 df_den = self.output_node.rn_df
    #                 f_value = c_snr[_idx_ch, _i] + 1
    #                 tests.append(
    #                     FpmTest(df_1=df_num,
    #                             df_2=df_den,
    #                             f=f_value,
    #                             f_critic=f.ppf(1 - self.output_node.alpha, df_num, df_den),
    #                             p_value=1 - f.cdf(f_value, df_num, df_den),
    #                             rn=self.output_node.rn[_idx_ch],
    #                             snr=c_snr[_idx_ch, _i],
    #                             time_ini=_ini / self.output_node.fs,
    #                             time_end=_end / self.output_node.fs,
    #                             n_epochs=n_trials[_b],
    #                             channel=_ch.label).__dict__
    #                 )
    #     out = pd.DataFrame(tests)
    #     return out

    def get_roi_samples(self):
        if self.roi_windows is not None:
            roi_samples = np.array([_roi.get_samples_interval(fs=self.input_node.fs) for _roi in self.roi_windows])
        else:
            roi_samples = [np.array([0, self.output_node.data.shape[0]])]

        return roi_samples


class AverageEpochsFrequencyDomain(InputOutputProcess):
    def __init__(self,
                 input_process=InputOutputProcess,
                 n_tracked_points: int = 256,
                 block_size: int = 5,
                 test_frequencies: np.array = None,
                 n_fft: int = None,
                 weighted_average=True,
                 weight_frequencies: np.array = None,
                 delta_frequency: u.Quantity = 5. * u.Hz,
                 power_line_frequency: u.Quantity = 50 * u.Hz,
                 **kwargs):
        """
        This InputOutputProcess average epochs in the frequency domain
        :param input_process: InputOutputProcess Class
        :param n_tracked_points: number of equally spaced points over time used to estimate residual noise
        :param block_size: number of trials that will be stack together to estimate the residual noise
        :param test_frequencies: numpy array with frequencies that will be used to compute statistics (Hotelling test)
        :param weighted_average: bool indicating if weighted average is to be used, otherwise standard average is used
        :param weight_frequencies: numpy array with frequencies that will be used to estimate weights when
        weighted_average is activated
        :param delta_frequency: frequency size around each test_frequencies to estimate weights and noise
        :param power_line_frequency: frequency of local power line frequency. This will be used to prevent using
        this frequency or its multiples when performing frequency statistics
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(AverageEpochsFrequencyDomain, self).__init__(input_process=input_process, **kwargs)
        self.n_tracked_points = n_tracked_points
        self.block_size = block_size
        self.test_frequencies = set_default_unit(test_frequencies, u.Hz)
        self.weight_frequencies = set_default_unit(weight_frequencies, u.Hz)
        self.weighted_average = weighted_average
        self.delta_frequency = set_default_unit(delta_frequency, u.Hz)
        self.power_line_frequency = set_default_unit(power_line_frequency, u.Hz)

    def transform_data(self):
        # average processed data across epochs including frequency average
        if self.weight_frequencies is None and self.weighted_average:
            print('No specific frequency provided to estimate weights in the frequency-domain. Pooling across '
                  'test frequencies {:}. Results may be inaccurate if test frequencies contain no '
                  'relevant signal'.format(self.test_frequencies.to_string()))
            self.weight_frequencies = self.test_frequencies

        w_ave, snr, rn, by_freq_rn, by_freq_snr, w_fft, w, freq_samples, exact_frequencies = et_frequency_mean2(
            epochs=self.input_node.data,
            fs=self.input_node.fs,
            weighted_average=self.weighted_average,
            test_frequencies=self.weight_frequencies,
            delta_frequency=self.delta_frequency,
            block_size=self.block_size,
            power_line_frequency=self.power_line_frequency
        )

        hts = []
        for _idx, _f in enumerate(exact_frequencies):
            _freq_samples = freq_samples[_idx, :, :]
            h_samples = np.vstack((np.real(_freq_samples), np.imag(_freq_samples)))
            h_samples = h_samples.reshape((2, *freq_samples.shape[1::]))
            _spectral_amp = w_mean(epochs=_freq_samples.reshape((1, *freq_samples.shape[1::])),
                                   weights=w[0, :, :])
            _hts = hotelling_t_square_test(samples=h_samples, weights=w, channels=self.input_node.layout)
            for _idx_ch, _ht in enumerate(_hts):
                _ht.frequency_tested = _f.squeeze()
                _spectral_phase = np.angle(_spectral_amp[:, _idx_ch]).squeeze()
                _ht.mean_amplitude = np.abs(_spectral_amp[:, _idx_ch]).squeeze()
                _ht.mean_phase = _spectral_phase
            hts = hts + _hts
            # hotelling_t_square_test
        self.output_node.data = w_fft
        self.output_node.n_fft = self.input_node.data.shape[0]
        self.output_node.domain = Domain.frequency
        self.output_node.rn = rn
        self.output_node.snr = snr
        all_ht2 = [_h.__dict__ for _h in hts]
        self.output_node.statistical_tests = pd.DataFrame(all_ht2)
        self.output_node.peak_frequency = self.get_frequency_peaks_as_pandas(pd.DataFrame(all_ht2))

    @staticmethod
    def get_frequency_peaks_as_pandas(hotelling_tests: pd.DataFrame = None):
        f_peaks = []
        for _i, _s_ht in hotelling_tests.iterrows():
            _f_peak = EegPeak(channel=_s_ht.channel,
                              x=_s_ht.frequency_tested,
                              rn=_s_ht.rn,
                              amp=_s_ht.mean_amplitude,
                              amp_snr=_s_ht.snr,
                              significant=bool(_s_ht.p_value < 0.05),
                              peak_label="{:10.1f}".format(_s_ht.frequency_tested),
                              show_label=True,
                              positive=True,
                              domain=Domain.frequency,
                              spectral_phase=_s_ht.mean_phase)
            f_peaks.append(_f_peak.__dict__)
        _data_pd = pd.DataFrame(f_peaks)
        return _data_pd


class InterpolateData(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 ini_end_points: np.array = np.array([[], []]),
                 save_plots: bool = True,
                 n_plots: int = 9,
                 fig_format: str = '.png',
                 fontsize: float = 8,
                 idx_channels_to_plot: np.array = np.array([0]),
                 user_naming_rule: str = 'interpolation',
                 **kwargs):
        """
        This class will interpolate data on each channel at a between two points passed in ini_end_points
        :param input_process: an InputOutput process
        :param ini_end_points: a Nx2 numpy array with the interpolation points
        :param save_plots: bool, if true, figures will be generated and saved showing a zoomed progression of the interp
        :param n_plots: int: number of plots (with different zoom scales) to be generated
        :param fig_format: string indicating the format of the output figure (e.g. '.png' or '.pdf')
        :param fontsize: size of fonts in plot
        :param idx_channels_to_plot: np.array indicating the index of the channels to plot
        :param user_naming_rule: string indicating a user naming to be included in the figure file name
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(InterpolateData, self).__init__(input_process=input_process, **kwargs)
        self.ini_end_points = ini_end_points
        self.save_plots = save_plots
        self.n_plots = n_plots
        self.fig_format = fig_format
        self.fontsize = fontsize
        self.idx_channels_to_plot = idx_channels_to_plot
        self.user_naming_rule = user_naming_rule
        self.fontsize = fontsize

    def transform_data(self):
        if self.ini_end_points.size:
            print('Interpolating data')
            idx_delete = np.concatenate((np.where(self.ini_end_points[:, 0] >
                                                  self.input_node.data.shape[0] - 1)[0],
                                         np.where(self.ini_end_points[:, 1] >
                                                  self.input_node.data.shape[0] - 1)[0]))

            if np.any(idx_delete):
                self.ini_end_points = np.delete(self.ini_end_points, idx_delete, 0)
            self.output_node.data = self.input_node.data.copy()
            for _i in range(self.ini_end_points.shape[0]):
                _ini = self.ini_end_points[_i, 0]
                _end = self.ini_end_points[_i, 1]
                new_x = np.linspace(_ini, _end, num=_end - _ini + 1).astype(np.int)
                new_x_ar = np.tile(np.atleast_2d(new_x).T, (1, self.input_node.data.shape[1]))
                self.output_node.data[new_x, :] = ((new_x_ar - _ini) * (self.output_node.data[_end, :] -
                                                                        self.output_node.data[_ini, :]) /
                                                   (_end - _ini) + self.output_node.data[_ini, :])
            if self.save_plots:
                for _idx_channel in self.idx_channels_to_plot:
                    fig, ax = plt.subplots(1, 1)
                    ax.plot(self.input_node.x, self.input_node.data[:, _idx_channel])
                    ax.plot(self.input_node.x, self.output_node.data[:, _idx_channel])
                    ax.plot(self.input_node.x[self.ini_end_points[:, 0]],
                            self.output_node.data[self.ini_end_points[:, 0], _idx_channel],
                            'o', color='k', markersize=2)
                    ax.plot(self.input_node.x[self.ini_end_points[:, 1]],
                            self.output_node.data[self.ini_end_points[:, 1], _idx_channel],
                            'o', color='r', markersize=2)
                    m_point = self.ini_end_points.shape[0] // 2
                    figure_dir_path = self.input_node.paths.figures_current_dir
                    figure_basename = self.__class__.__name__ + '_' + self.user_naming_rule
                    for _i in range(self.n_plots):
                        _label = self.input_node.layout[_idx_channel].label
                        _fig_path = figure_dir_path + os.path.sep + _label + '_' + figure_basename + '_{:}{:}'.format(
                            _i, self.fig_format)
                        _ini = max(0, m_point - 2 * 2 ** _i)
                        _end = min(self.ini_end_points.shape[0], m_point + 2 * 2 ** _i)
                        ax.set_xlim(self.input_node.x[self.ini_end_points[_ini, 0]],
                                    self.input_node.x[self.ini_end_points[_end, 0]])
                        ax.autoscale_view()
                        ax.set_xlabel('Time [{:}]'.format(self.input_node.x.unit), fontsize=self.fontsize)
                        ax.set_ylabel('Amplitude [{:}]'.format(self.input_node.data.unit), fontsize=self.fontsize)
                        ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
                        fig.tight_layout()
                        fig.savefig(_fig_path)
                        print('saving interpolated figures in {:}'.format(_fig_path))
                    plt.close(fig)


class PeriodicInterpolation(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 interpolation_rate=None,
                 interpolation_width=0.0,
                 interpolation_offset=0.0,
                 event_code=0.0,
                 user_naming_rule='',
                 **kwargs):
        """
        This class will interpolate data on each channel at a given rate using as a reference time the position of
        trigger events
        :param input_process: InputOutputProcess Class
        :param interpolation_rate: rate (in Hz) to interpolate data points
        :param interpolation_width: the width (in sec) of the interpolation region
        :param interpolation_offset: an offset (in sec) in reference to trigger events
        :param event_code: the event code that will be use as reference to start interpolating
        :param user_naming_rule: string indicating a user naming to be included in the figure file name
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(PeriodicInterpolation, self).__init__(input_process=input_process, **kwargs)
        self.interpolation_rate = interpolation_rate
        self.interpolation_width = interpolation_width
        self.interpolation_offset = interpolation_offset
        self.event_code = event_code
        self.user_naming_rule = user_naming_rule

    def transform_data(self):
        _events = self.input_node.events.get_events(self.event_code)
        _event_times = [_e.time_pos for _e in _events]

        _ini_points = np.array([])
        for _start, _end in zip(_event_times[:-1], _event_times[1::]):
            if self.interpolation_rate is not None:
                _ini_points = np.concatenate((_ini_points, np.arange(_start, _end, 1. / self.interpolation_rate)))
            else:
                _ini_points = np.append(_ini_points, _start)

        if self.interpolation_rate is not None:
            _ini_points = np.concatenate((_ini_points, np.arange(_event_times[-1],
                                                                 self.input_node.data.shape[0] /
                                                                 self.input_node.fs,
                                                                 1. / self.interpolation_rate)))
        _ini_points -= self.interpolation_offset
        _end_points = _ini_points + self.interpolation_width
        ini_end_points = (np.array([_ini_points, _end_points]).T * self.input_node.fs).astype(np.int)
        interp_data = InterpolateData(self.input_process, ini_end_points=ini_end_points,
                                      user_naming_rule=self.user_naming_rule)
        interp_data.run()
        self.output_node = interp_data.output_node


class InduceResponse(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, weighted_average=True, n_tracked_points=256, block_size=5,
                 roi_windows=None, **kwargs):
        super(InduceResponse, self).__init__(input_process=input_process, **kwargs)
        self.weighted_average = weighted_average
        self.n_tracked_points = n_tracked_points
        self.block_size = block_size
        self.roi_windows = roi_windows

    def transform_data(self):
        trials_abs_w_ave, w, rn, cum_rn, snr, cum_snr, s_var, w_fft = \
            et_mean(epochs=np.abs(self.input_node.data),
                    block_size=max(self.block_size, 5),
                    samples_distance=int(max(self.input_node.data.shape[0] // self.n_tracked_points, 10)),
                    roi_windows=self.roi_windows,
                    weighted=self.weighted_average
                    )
        w_ave, w, rn, cum_rn, snr, cum_snr, s_var, w_fft = \
            et_mean(epochs=self.input_node.data,
                    block_size=max(self.block_size, 5),
                    samples_distance=int(max(self.input_node.data.shape[0] // self.n_tracked_points, 10)),
                    roi_windows=self.roi_windows,
                    weighted=self.weighted_average
                    )
        self.output_node.data = trials_abs_w_ave - np.abs(w_ave)
        self.output_node.rn = rn
        self.output_node.cum_rn = cum_rn
        self.output_node.snr = snr
        self.output_node.cum_snr = cum_snr
        self.output_node.s_var = s_var


class AverageTimeFrequencyResponse(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 time_window: u.Quantity = 0.3 * u.s,
                 sample_interval: u.Quantity = 0.004 * u.s,
                 topographic_channels=np.array([]),
                 title: str = '',
                 plot_x_lim=None,
                 plot_y_lim=None,
                 times=np.array([]),
                 fig_format='.png',
                 fontsize=12,
                 user_naming_rule: str = '',
                 spec_thresh=4,
                 average_mode='magnitude',
                 **kwargs):
        super(AverageTimeFrequencyResponse, self).__init__(input_process=input_process, **kwargs)
        self.time_window = time_window
        self.sample_interval = sample_interval
        self.frequency = None
        self.time = None
        self.topographic_channels = topographic_channels
        self.title = title
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.fig_format = fig_format
        self.fontsize = fontsize
        self.user_naming_rule = user_naming_rule
        self.times = times
        self.time_window = time_window
        self.sample_interval = sample_interval
        self.spec_thresh = spec_thresh
        self.average_mode = average_mode

    def transform_data(self):
        power, time, freqs = et_average_time_frequency_transformation(epochs=self.input_node.data,
                                                                      fs=self.input_node.fs,
                                                                      time_window=self.time_window,
                                                                      sample_interval=self.sample_interval,
                                                                      average_mode=self.average_mode
                                                                      )
        self.output_node.data = power
        self.output_node.x = time
        self.output_node.y = freqs
        self.output_node.domain = Domain.time_frequency


class PlotTimeFrequencyData(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 topographic_channels=np.array([]),
                 title='',
                 plot_x_lim=None,
                 plot_y_lim=None,
                 times=np.array([]),
                 fig_format='.png',
                 fontsize=12,
                 user_naming_rule='',
                 spec_thresh=4,
                 **kwargs):
        super(PlotTimeFrequencyData, self).__init__(input_process=input_process, **kwargs)
        self.topographic_channels = topographic_channels
        self.title = title
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.fig_format = fig_format
        self.fontsize = fontsize
        self.user_naming_rule = user_naming_rule
        self.times = times
        self.spec_thresh = spec_thresh

    def transform_data(self):
        assert self.input_node.domain == Domain.time_frequency, 'input should be a time-frequency transformed data'

        eegpt.plot_eeg_time_frequency_power(ave_data=self.input_node,
                                            time=self.input_node.x,
                                            frequency=self.input_node.y,
                                            eeg_topographic_map_channels=self.topographic_channels,
                                            figure_dir_path=self.input_node.paths.figures_current_dir,
                                            figure_basename='{:}{:}'.format(
                                                self.input_process.__class__.__name__ + '_',
                                                self.user_naming_rule),
                                            time_unit='s',
                                            amplitude_unit='uV',
                                            times=self.times,
                                            title=self.title,
                                            x_lim=self.plot_x_lim,
                                            y_lim=self.plot_y_lim,
                                            fig_format=self.fig_format,
                                            fontsize=self.fontsize,
                                            spec_thresh=self.spec_thresh
                                            )


class PlotWaveforms(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 overlay: [InputOutputProcess] = None,
                 ch_to_plot: np.array = None,
                 title: str = '',
                 plot_x_lim: [float, float] = None,
                 plot_y_lim: [float, float] = None,
                 offset_step: u.quantity.Quantity = None,
                 return_figures: bool = False,
                 show_following_stats: [str] = None,
                 fig_format: str = '.png',
                 fontsize: float = 12,
                 user_naming_rule: str = '',
                 save_to_file: bool = True,
                 show_peaks: bool = True,
                 show_labels: bool = True,
                 **kwargs):
        """
        This InputOutputProcess plots all channel waveforms in a single plot and saves them.
        :param input_process: an InputOutput process
        :param ch_to_plot: numpy array with index or labels of channels to plot
        :param overlay: list of InputOutputProcess to overlay
        :param title: title of the plot
        :param plot_x_lim: x limits of the plot
        :param plot_y_lim: y limits of the plot
        :param offset_step: offset between different channels
        :param return_figures: If true, handle to figure will be passed to self.figures
        :param show_following_stats: list of string indicating which parameters found in statistical_tests are shown
        :param fig_format: string indicating the format of the output figure (e.g. '.png' or '.pdf')
        :param fontsize: size of fonts in plot
        :param idx_channels_to_plot: np.array indicating the index of the channels to plot
        :param user_naming_rule: string indicating a user naming to be included in the figure file name
        :param save_to_file: if True, figure will be saved
        :param show_peaks: if True, detected peaks will be shown
        :param show_labels: if True, peak labels will be shown
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(PlotWaveforms, self).__init__(input_process=input_process, **kwargs)
        self.ch_to_plot = ch_to_plot
        self.title = title
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.offset_step = set_default_unit(offset_step, u.uV)
        self.fig_format = fig_format
        self.fontsize = fontsize
        self.user_naming_rule = user_naming_rule
        self.overlay = overlay
        self.show_following_stats = show_following_stats
        self.return_figures = return_figures
        self.save_to_file = save_to_file
        self.show_peaks = show_peaks
        self.show_labels = show_labels

    def transform_data(self):
        figure_dir_path = self.input_node.paths.figures_current_dir
        figure_basename = self.input_process.__class__.__name__ + '_' + self.user_naming_rule
        to_plot = [self.input_node]
        if self.overlay is not None:
            to_plot += [_in.output_node for _in in self.overlay]

        figure = eegpt.plot_single_channels(ave_data=to_plot,
                                            channels=self.ch_to_plot,
                                            figure_dir_path=figure_dir_path,
                                            figure_basename=figure_basename,
                                            title=self.title,
                                            x_lim=self.plot_x_lim,
                                            y_lim=self.plot_y_lim,
                                            offset_step=self.offset_step,
                                            fig_format=self.fig_format,
                                            fontsize=self.fontsize,
                                            show_following_stats=self.show_following_stats,
                                            save_to_file=self.save_to_file,
                                            show_peaks=self.show_peaks,
                                            show_labels=self.show_labels
                                            )
        if self.return_figures:
            self.figures = figure
        else:
            plt.close(figure)
            gc.collect()


class AverageFrequencyPower(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 topographic_channels=np.array([]),
                 title='',
                 plot_x_lim=None,
                 plot_y_lim=None,
                 times=np.array([]),
                 fig_format='.png',
                 fontsize=12,
                 user_naming_rule='',
                 **kwargs):
        super(AverageFrequencyPower, self).__init__(input_process=input_process, **kwargs)
        self.frequency = None
        self.time = None
        self.topographic_channels = topographic_channels
        self.title = title
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.fig_format = fig_format
        self.fontsize = fontsize
        self.user_naming_rule = user_naming_rule
        self.times = times

    def transform_data(self):
        power, freqs = et_average_frequency_transformation(epochs=self.input_node.data,
                                                           fs=self.input_node.fs,
                                                           ave_mode='magnitude'
                                                           )
        self.output_node.data = power


class PeakDetectionTimeDomain(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, time_peak_windows=np.array([TimePeakWindow]),
                 peak_to_peak_measures=np.array([PeakToPeakMeasure]), **kwargs):
        super(PeakDetectionTimeDomain, self).__init__(input_process=input_process, **kwargs)
        self.time_peak_windows = time_peak_windows
        self.peak_to_peak_measures = peak_to_peak_measures

    def transform_data(self):
        peak_containers, amplitudes = detect_peaks_and_amplitudes(
            data_node=self.input_node,
            time_peak_windows=self.time_peak_windows,
            eeg_peak_to_peak_measures=self.peak_to_peak_measures)

        _data_pd = pd.concat([_peaks.to_pandas() for _peaks in peak_containers])
        _amps_pd = pd.concat([_amp.to_pandas() for _amp in amplitudes])
        self.output_node.data = self.input_node.data
        self.output_node.peak_times = _data_pd
        self.output_node.peak_to_peak_amplitudes = _amps_pd


class PlotTopographicMap(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 topographic_channels: np.array([str]) = np.array([]),
                 title: str = '',
                 plot_x_lim: list = None,
                 plot_y_lim: list = None,
                 times: np.array = np.array([]),
                 fig_format: str = '.png',
                 fontsize: float = 12,
                 user_naming_rule: str = '',
                 return_figures: bool = False,
                 **kwargs
                 ):
        """
        Plot topographic maps of figures at desired times. If the input_process contains time or frequency peaks from
        PeakDetectionTimeDomain or tests such as FTest, AverageEpochsFrequencyDomain, or PhaseLockingValue,
        topographic maps for those peaks will be also shown
        :param input_process: an InputOutput process
        :param topographic_channels: a numpy array (integers index or string with channel labels) indicating channels to
        be plotted
        :param title: string with the desired figure title
        :param plot_x_lim: list with minimum and maximum horizontal range of x axis
        :param plot_y_lim: list with minimum and maximum vertical range of y axis
        :param times: numeric array with times to show topographic maps
        :param fig_format: string indicating the format of the figure being saved
        :param fontsize: float indicating the size of the fonts
        :param user_naming_rule: string containing an extra text to be included in the name of the figure
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(PlotTopographicMap, self).__init__(input_process=input_process, **kwargs)
        self.topographic_channels = topographic_channels
        self.title = title
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.fig_format = fig_format
        self.fontsize = fontsize
        self.user_naming_rule = user_naming_rule
        self.times = times
        self.return_figures = return_figures

    def transform_data(self):
        _figure_base_name = self.input_process.__class__.__name__ + self.user_naming_rule
        figures = eegpt.plot_eeg_topographic_map(ave_data=self.input_node,
                                                 eeg_topographic_map_channels=self.topographic_channels,
                                                 figure_dir_path=self.input_node.paths.figures_current_dir,
                                                 figure_basename=_figure_base_name,
                                                 times=self.times,
                                                 domain=self.input_node.domain,
                                                 title=self.title,
                                                 x_lim=self.plot_x_lim,
                                                 y_lim=self.plot_y_lim,
                                                 fig_format=self.fig_format,
                                                 fontsize=self.fontsize,
                                                 return_figures=self.return_figures)
        self.figures = figures


class AverageSpectrogram(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 topographic_channels=np.array([]),
                 title='',
                 plot_x_lim=None,
                 plot_y_lim=None,
                 times=np.array([]),
                 fig_format='.png',
                 fontsize=12,
                 user_naming_rule='',
                 time_window=2.0,
                 sample_interval=0.004,
                 spec_thresh=4
                 ):
        super(PlotSpectrogram, self).__init__(input_process=input_process)
        self.topographic_channels = topographic_channels
        self.title = title
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.fig_format = fig_format
        self.fontsize = fontsize
        self.user_naming_rule = user_naming_rule
        self.times = times
        self.time_window = time_window
        self.sample_interval = sample_interval
        self.spec_thresh = spec_thresh

    def transform_data(self):
        power, freqs = et_average_frequency_transformation(epochs=self.input_node.data,
                                                           fs=self.input_node.fs,
                                                           ave_mode='magnitude'
                                                           )
        self.output_node.data = power


class PlotSpectrogram(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 topographic_channels=np.array([]),
                 title='',
                 plot_x_lim=None,
                 plot_y_lim=None,
                 times=np.array([]),
                 fig_format='.png',
                 fontsize=12,
                 user_naming_rule='',
                 time_window=2.0,
                 sample_interval=0.004,
                 spec_thresh=4
                 ):
        super(PlotSpectrogram, self).__init__(input_process=input_process)
        self.topographic_channels = topographic_channels
        self.title = title
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.fig_format = fig_format
        self.fontsize = fontsize
        self.user_naming_rule = user_naming_rule
        self.times = times
        self.time_window = time_window
        self.sample_interval = sample_interval
        self.spec_thresh = spec_thresh

    def transform_data(self):
        eegpt.plot_eeg_time_frequency_transformation(ave_data=self.input_node,
                                                     eeg_topographic_map_channels=self.topographic_channels,
                                                     figure_dir_path=self.input_node.paths.figure_basename_path,
                                                     figure_basename='{:}{:}'.format(
                                                         self.input_process.__class__.__name__ + '_',
                                                         self.user_naming_rule),
                                                     time_unit='s',
                                                     amplitude_unit='uV',
                                                     times=self.times,
                                                     domain=self.input_node.domain,
                                                     title=self.title,
                                                     x_lim=self.plot_x_lim,
                                                     y_lim=self.plot_y_lim,
                                                     fig_format=self.fig_format,
                                                     fontsize=self.fontsize,
                                                     time_window=self.time_window,
                                                     sample_interval=self.sample_interval,
                                                     spec_thresh=self.spec_thresh
                                                     )


class DeEpoch(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess, de_mean=True, **kwargs):
        """
        This function will convert a n*m*p matrix into an (n*p) * m matrix
        :param input_process: InputOutputProcess Class
        :param de_mean: whether data should be demeaned before converting
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(DeEpoch, self).__init__(input_process=input_process, **kwargs)
        self.de_mean = de_mean

    def transform_data(self):
        _data = np.array(self.input_node.data.data)
        # if self.de_mean:
        #     _data -= np.mean(_data, axis=0)
        for _i in range(1, _data.shape[2]):
            _data[:, :, _i] = _data[:, :, _i] - (_data[0, :, _i] - _data[-1, :, _i - 1])

        self.output_node.data = np.reshape(np.transpose(_data, [0, 2, 1]), [-1, _data.shape[1]], order='F')
        events = []
        for _i in range(_data.shape[2]):
            events.append(SingleEvent(code=1,
                                      time_pos=_i * _data.shape[0] / self.input_node.fs,
                                      dur=0))
        events = Events(events=np.array(events))
        self.output_node.events = events


class HotellingT2Test(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 block_time: u.quantity.Quantity = 0.040 * u.s,
                 detrend_data=False,
                 roi_windows: np.array(TimeROI) = None,
                 weight_data: bool = True,
                 block_size: int = 5,
                 **kwargs) -> InputOutputProcess:
        """
        This class computes the statistical significance of a region of interest using a Hotelling test.
        The time region where a response is expected is tested to assess whether the linear combination of data points
        within this region is significantly different of a random linear combination of points.

        :param input_process: InputOutputProcess Class
        :param block_time: the length (in secs) of each average window within a region of interest (roi). A ROI will be
        divided in several points of duration block_time. The average of each block_time will represent a sample passed
        for the statistical test
        :param detrend_data: whether to remove or not any linear component on each trial before performing the test
        :param roi_windows: a region of windows containing the response of interest
        :param weight_data: if True, estimations are based on weights from input node. If empty, weights are
        estimated from weighted average.
        :param block_size: number of epochs used to estimate weights when weight_data is True
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(HotellingT2Test, self).__init__(input_process=input_process, **kwargs)
        self.block_time = set_default_unit(block_time, u.s)
        self.detrend_data = detrend_data
        self.roi_windows = roi_windows
        self.weight_data = weight_data
        self.block_size = block_size

    def transform_data(self):
        block_size = int(self.block_time * self.input_node.fs)
        # by default we will use whatever weights are in the input_node
        weights = self.input_node.w
        # if no weights are passed, we compute them if required
        if self.weight_data and weights is None:
            # compute weighted average to extract weights
            _, weights, _, _, _, _ = et_mean(epochs=self.input_node.data,
                                             block_size=self.block_size,
                                             samples_distance=int(max(self.input_node.data.shape[0] // 256, 10)),
                                             weighted=True
                                             )
        h_tests = []
        for _roi_idx, _roi in enumerate(self.roi_windows):
            _samples = _roi.get_samples_interval(fs=self.input_node.fs)
            data = self.input_node.data.copy()
            # remove linear trend
            _ini = _samples[0]
            _end = _samples[1]
            if self.detrend_data:
                x = np.expand_dims(np.arange(0, data[_ini:_end, :, :].shape[0]), axis=1)
                for _idx in np.arange(data.shape[2]):
                    _ini_dt = np.maximum(0, _ini - int(self.fs * 0.1))
                    _end_dt = np.minimum(data.shape[0], _end + int(self.fs * 0.1))
                    _subset = data[_ini_dt:_end_dt, :, _idx].copy()
                    x_dt = np.expand_dims(np.arange(0, data[_ini_dt:_end_dt, :, :].shape[0]), axis=1)
                    regression = LinearRegression()
                    regression.fit(x_dt, _subset)
                    data[_ini:_end, :, _idx] -= regression.predict(x)
            # remove mean
            data[_ini:_end, :, :] = data[_ini:_end, :, :] - np.mean(data[_ini:_end, :, :], axis=0)
            block_size = max(0, min(block_size, _samples[1] - _samples[0]))
            n_blocks = np.floor((_samples[1] - _samples[0]) / block_size).astype(np.int)
            samples = np.array([np.mean(data[_ini + _i * block_size: _ini + (_i + 1) * block_size, :, :], 0)
                                for _i in range(n_blocks)]) * data.unit
            _roi_h_tests = hotelling_t_square_test(samples,
                                                   weights=weights,
                                                   channels=self.input_node.layout,
                                                   **_roi.__dict__)
            h_tests = h_tests + _roi_h_tests
        self.output_node.statistical_tests = pd.DataFrame([_ht.__dict__ for _ht in h_tests])


class FTest(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 delta_frequency: u.Quantity = 10. * u.Hz,
                 test_frequencies: np.array = None,
                 alpha: float = 0.05,
                 power_line_frequency: u.Quantity = 50 * u.Hz,
                 ignored_frequency_width: u.Quantity = 1 * u.Hz,
                 n_fft: int = None,
                 **kwargs) -> InputOutputProcess:
        """
        This class computes a FTest in the frequency domain of a signal.
        :param input_process: InputOutputProcess Class
        :param delta_frequency: the length in Hz around each target frequency to compute statistics
        :param test_frequencies: a numpy array with the frequencies that will be tested
        :param alpha: level to assess significance of the test
        :param power_line_frequency: frequency of local power line frequency. This will be used to prevent using
        this frequency or its multiples when performing the FTest
        :param ignored_frequency_width: widh, in Hz, indicating the with around the target frequency that will be
        ignored when estimating the F-ratio. Neighbors frequencies within +- this value will not be considered for the
        F-test.
        :param n_fft: number of points to perform fft transformation
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(FTest, self).__init__(input_process=input_process, **kwargs)
        self.delta_frequency = set_default_unit(delta_frequency, u.Hz)
        self.ignored_frequency_width = set_default_unit(ignored_frequency_width, u.Hz)
        self.test_frequencies = set_default_unit(test_frequencies, u.Hz)
        self.fft_frequencies: np.array = None
        self.power_line_frequency = power_line_frequency
        self.alpha = alpha
        self.n_fft = n_fft

    def transform_data(self):
        if self.input_node.domain == Domain.time:
            if self.n_fft is None:
                self.n_fft = self.input_node.data.shape[0]
            fft = pyfftw.builders.rfft(self.input_node.data,
                                       overwrite_input=False,
                                       planner_effort='FFTW_ESTIMATE',
                                       axis=0,
                                       threads=multiprocessing.cpu_count(), n=self.n_fft)
            w_fft = fft() * self.input_node.data.unit
            w_fft /= self.input_node.data.shape[0] / 2
            self.fft_frequencies = np.arange(w_fft.shape[0]) * self.input_node.fs / self.n_fft
        else:
            self.n_fft = self.input_node.n_fft
            w_fft = self.input_node.data
            self.fft_frequencies = self.input_node.x

        results = [[]] * self.input_node.data.shape[1]
        all_makers = np.array([])
        for _freq in self.test_frequencies:
            _target_bin = self.freq_to_samples(_freq)
            # compute array of bins around target frequency
            _ini_bin = np.maximum(self.freq_to_samples(_freq - self.delta_frequency), 0)
            _end_bin = np.minimum(self.freq_to_samples(_freq + self.delta_frequency), w_fft.shape[0] - 1)
            _neighbours_idx_left = np.arange(_ini_bin, _target_bin)
            _neighbours_idx_right = np.arange(_target_bin + 1, _end_bin + 1)
            _neighbours_idx = np.concatenate((
                _neighbours_idx_left,
                _neighbours_idx_right))
            # compute array of bins around target frequency
            _ini_ignore = np.maximum(self.freq_to_samples(_freq - self.ignored_frequency_width), 0)
            _end_ignore = np.minimum(self.freq_to_samples(_freq + self.ignored_frequency_width), w_fft.shape[0] - 1)
            _neighbours_to_ignore_idx = np.concatenate((
                np.arange(_ini_ignore, _target_bin),
                np.arange(_target_bin + 1, _end_ignore + 1)))
            # compute power line frequencies
            _power_idx = np.array(
                [self.freq_to_samples(_power_freq) for _power_freq in
                 self.power_line_frequency * np.arange(1,
                                                       np.round(0.5 * self.input_node.fs / self.power_line_frequency))])
            # remove bins
            _neighbours_idx = np.setdiff1d(_neighbours_idx, _neighbours_to_ignore_idx)
            _neighbours_idx = np.setdiff1d(_neighbours_idx, _power_idx)

            _n = _neighbours_idx.size
            _f_critic = f.ppf(1 - self.alpha, 2, 2 * _n)
            _rns = np.sqrt(np.sum(np.abs(w_fft[_neighbours_idx, :]) ** 2, axis=0) / _n)
            _f_tests = np.abs(w_fft[_target_bin, :]) ** 2 / _rns ** 2.0

            for _ch, _f in enumerate(_f_tests):
                _d1 = 2
                _d2 = 2 * _n
                _p_values = 1 - f.cdf(_f, _d1, _d2)
                _snr = np.maximum(_f - 1, 0.0)
                _test_out = FrequencyFTest(frequency_tested=_freq,
                                           df_1=_d1,
                                           df_2=_d2,
                                           f=_f,
                                           p_value=_p_values,
                                           spectral_magnitude=np.abs(w_fft[_target_bin, _ch]),
                                           spectral_phase=np.angle(w_fft[_target_bin, _ch]),
                                           rn=_rns[_ch],
                                           snr=_snr,
                                           snr_db=10 * np.log10(_snr) if _snr > 0.0 else
                                           -np.Inf * u.dimensionless_unscaled,
                                           f_critic=_f_critic)
                results[_ch] = np.append(results[_ch], [_test_out])
            all_markers_l = [Marker(x_ini=self.fft_frequencies[np.min(_neighbours_idx_left)],
                                    x_end=self.fft_frequencies[np.max(_neighbours_idx_left)],
                                    channel=_ch.label) for _ch in self.input_node.layout]
            all_markers_r = [Marker(x_ini=self.fft_frequencies[np.min(_neighbours_idx_right)],
                                    x_end=self.fft_frequencies[np.max(_neighbours_idx_right)],
                                    channel=_ch.label) for _ch in self.input_node.layout]
            all_makers = np.concatenate((all_makers, all_markers_l, all_markers_r))

        # self.output_node.ht_tests = self.get_ht_tests_as_pandas(h_tests)
        self.output_node.data = w_fft
        self.output_node.n_fft = self.n_fft
        self.output_node.domain = Domain.frequency
        self.output_node.statistical_tests = self.get_f_tests_as_pandas(results)
        self.output_node.peak_frequency = self.get_frequency_peaks_as_pandas(f_tests=results)

        self.output_node.markers = pd.DataFrame([_m.__dict__ for _m in all_makers])

    def get_frequency_peaks_as_pandas(self, f_tests: [np.array(FrequencyFTest)] = None):
        f_peaks = []
        for _i, (_f_ch, _ch) in enumerate(zip(f_tests, self.input_node.layout)):
            for _s_ft in _f_ch:
                _s_ft.label = _ch.label
                _f_peak = EegPeak(channel=_ch.label,
                                  x=_s_ft.frequency_tested,
                                  rn=_s_ft.rn,
                                  amp=_s_ft.spectral_magnitude,
                                  amp_snr=_s_ft.snr,
                                  significant=bool(_s_ft.p_value < 0.05),
                                  peak_label="{:10.1f}".format(_s_ft.frequency_tested),
                                  show_label=True,
                                  positive=True,
                                  domain=Domain.frequency,
                                  spectral_phase=_s_ft.spectral_phase)
                f_peaks.append(_f_peak.__dict__)
        _data_pd = pd.DataFrame(f_peaks)
        return _data_pd

    def freq_to_samples(self, value: np.array) -> np.array:
        if isinstance(value, float) or value.size == 1:
            value = [value]
        out = np.array([])
        for _v in value:
            out = np.append(out, np.argmin(np.abs(self.fft_frequencies - _v))).astype(np.int)
        return np.squeeze(out)

    def get_f_tests_as_pandas(self, f_tests: [np.array(HotellingTSquareFrequencyTest)] = None):
        f_peaks = []
        for _i, (_ht_ch, _ch) in enumerate(zip(f_tests, self.input_node.layout)):
            for _s_ht in _ht_ch:
                _s_ht.channel = _ch.label
                f_peaks.append(_s_ht.__dict__)
        _data_pd = pd.DataFrame(f_peaks)
        return _data_pd


class AppendGFPChannel(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 channel_label: str = 'GFP',
                 **kwargs):
        """
        This InputOutputProcess will append the global field power (standard deviation across all channels)
        :param input_process: InputOutputProcess Class
        :param channel_label: name of new channel in layout
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(AppendGFPChannel, self).__init__(input_process=input_process, **kwargs)
        self.channel_label = channel_label

    def transform_data(self):
        data = self.input_node.data
        weights = self.input_node.w
        original_ndim = data.ndim
        # if data is an average rather than single epochs we add an extra dimension
        if original_ndim == 2:
            data = data[:, :, None]
            weights = np.ones(data.shape) * u.dimensionless_unscaled
        if weights is None:
            weights = np.ones(data.shape) * u.dimensionless_unscaled
        average = w_mean(data, weights=weights, keepdims=True, axis=1)
        variance = np.sum(weights * (data - average) ** 2,
                          axis=1, keepdims=True) / np.sum(weights, axis=1, keepdims=True)
        new_channel_data = np.sqrt(variance)
        # remove mean to center at gfp around zero, otherwise statistical tests will be always significant
        new_channel_data -= np.mean(new_channel_data, axis=0, keepdims=True)
        self.output_node.data = data
        self.output_node.append_new_channel(new_data=new_channel_data,
                                            layout_label=self.channel_label)
        if original_ndim == 2:
            self.output_node.data = np.squeeze(self.output_node.data)
        if weights is not None:
            new_w = np.ones((weights.shape[0], 1, weights.shape[2])) * weights.unit
            self.output_node.w = np.hstack((weights, new_w))


class AppendChannels(InputOutputProcess):
    def __init__(self, new_data: np.array = None,
                 channel_labels: [str] = None,
                 **kwargs):
        """
        This InputOutputProcess will append the global field power (standard deviation across all channels)
        :param input_process: InputOutputProcess Class
        :param channel_label: name of new channel in layout
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(AppendGFPChannel, self).__init__(input_process=None, **kwargs)
        self.channel_labels = channel_labels
        self.new_data = new_data

    def transform_data(self):
        data = self.input_node.data
        assert data.shape[0] == self.new_data.shape[0]
        assert len(self.channel_labels) == self.new_data.shape[1]
        self.output_node.data = data
        for _data, channel in zip(self.new_data, self.channel_labels):
            self.output_node.append_new_channel(new_data=self.new_data,
                                                layout_label=self.channel_labels)


class RegressOutArtifact(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 event_code: float = None,
                 artifact_window: float = 0.01,
                 alternating_polarity: bool = False,
                 stimulus_waveform: np.array = None,
                 method: str = 'regression',
                 **kwargs):
        """
        This InputOutputProcess will remove an artifact which has the shape of the input stimulus_waveform
        :param input_process: InputOutputProcess Class
        :param event_code: event code where the stimulus waveform starts
        :param artifact_window: time from onset where we only expect artifacts (free of neural response)
        :param alternating_polarity: indicate whether the paradigm used an alternating polarity scheme. the
        stimulus_waveform will be alternating in polarity accord
        :param stimulus_waveform: single column numpy array with the target waveform artifact
        :param method: either 'regression' or 'xcorr'. A regression model or an scaled amplitude based on cross
        correlation will be used to estimate the amount of artifact
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(RegressOutArtifact, self).__init__(input_process=input_process, **kwargs)
        self.event_code = event_code
        self.artifact_window = set_default_unit(artifact_window, u.s)
        self.alternating_polarity = alternating_polarity
        self.stimulus_waveform = set_default_unit(stimulus_waveform, u.uV)
        self.method = method

    def transform_data(self):
        data = self.input_node.data.copy()
        # if event code is given, convolve source with dirac train to generate entire signal
        subset_idx = np.arange(0, int(self.input_node.fs * self.artifact_window))
        if self.event_code is not None:
            _events_index = self.input_node.events.get_events_index(code=self.event_code, fs=self.input_node.fs)
            events = np.zeros(data.shape[0])
            events[_events_index] = 1
            if self.alternating_polarity:
                events[_events_index[1: -1: 2]] = -1
            #  convolve source with triggers
            source = filt_data(input_data=self.stimulus_waveform, b=events.flatten(), mode='full', onset_padding=False)
            source = source[0: data.shape[0]]
        else:
            source = self.stimulus_waveform[0: data.shape[0]]

        if self.method == 'regression':
            for _idx in np.arange(data.shape[1]):
                regression = LinearRegression()
                regression.fit(source[subset_idx], data[subset_idx, _idx])
                data[:, _idx] -= regression.predict(source) * source.unit
        if self.method == 'xcorr':
            if data.ndim == 2:
                transmission_index = data[subset_idx, :].T.dot(source[subset_idx]) / np.expand_dims(
                    np.sum(np.square(source[subset_idx])), axis=0)
                data -= np.tile(np.atleast_2d(source), (1, data.shape[1])) * transmission_index.T
            if data.ndim == 3:
                ave_data = np.mean(data, 2)
                transmission_index = ave_data[subset_idx, :].T.dot(source[subset_idx]) / np.expand_dims(
                    np.sum(np.square(source[subset_idx])), axis=0)
                data -= np.tile(np.atleast_3d(source * transmission_index.T), (1, 1, data.shape[2]))

        if self.method == 'xcorr_by_event':
            reconstructed = np.zeros(data.shape)
            # we recreate a peak template
            _template = self.stimulus_waveform
            for _idx in _events_index:
                _current_template = _template
                _ini = _idx
                _end = _idx + _template.shape[0]
                if _end > data.shape[0]:
                    _current_template = _template[0:-(_end - data.shape[0]), :]
                original = data[_ini: _end, :]
                transmission_index = original[subset_idx, :].T.dot(_current_template[subset_idx]) / np.expand_dims(
                    np.sum(np.square(_current_template[subset_idx])), axis=0)
                reconstructed[_ini: _end, :] = np.repeat(
                    _current_template,
                    data.shape[1],
                    axis=1) * transmission_index.T
            data -= reconstructed

        self.output_node.data = data


class RegressOutArtifactIR(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 stimulus_waveform: np.array = None,
                 ir_length: float = 0.01,
                 regularization_factor: float = 1,
                 **kwargs):
        """
        This InputOutputProcess will remove an artifact which has the shape of the input stimulus_waveform.
        Here, artifact is estimated from the impulse response. The latter is estimated by averaging individual
        impulse responses across epochs.
        :param input_process: InputOutputProcess Class
        stimulus_waveform will be alternating in polarity accord
        :param stimulus_waveform: single column numpy array with the target waveform artifact
        :param ir_length: estimated length in seconds of impulse response
        :param regularization_factor: This value is used to regularize the deconvolution based on Tikhonov
        regularization allow to estimate the transfer function when the input signal is sparse in frequency components
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(RegressOutArtifactIR, self).__init__(input_process=input_process, **kwargs)
        self.stimulus_waveform = stimulus_waveform
        self.ir_length = ir_length
        self.regularization_factor = regularization_factor
        self.recovered_artifact = None

    def transform_data(self):
        data = self.input_node.data.copy()
        # compute impulse response
        clean_data, _recovered_artifact = et_impulse_response_artifact_subtraction(
            data=data,
            stimulus_waveform=self.stimulus_waveform,
            ir_length=int(self.ir_length * self.input_node.fs),
            regularization_factor=self.regularization_factor)
        self.recovered_artifact = _recovered_artifact
        self.output_node.data = clean_data


class PhaseLockingValue(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 n_tracked_points: int = 256,
                 block_size: int = 5,
                 test_frequencies: np.array = None,
                 n_fft: int = None,
                 alpha=0.05,
                 weight_data: bool = True,
                 **kwargs):
        """
        This InputOutputProcess compute phase-locking value in the frequency domain
        :param input_process: InputOutputProcess Class
        :param n_tracked_points: number of equally spaced points over time used to estimate residual noise
        :param block_size: number of trials that will be stack together to estimate the residual noise
        :param test_frequencies: numpy array with frequencies that will be used to compute statistics (Hotelling test)
        :param n_fft: number of fft points
        :para, weight_data: If true, weights will be applied. If there are not weights, these will be calculated from
        weighted average.
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(PhaseLockingValue, self).__init__(input_process=input_process, **kwargs)
        self.n_tracked_points = n_tracked_points
        self.block_size = block_size
        self.test_frequencies = set_default_unit(np.unique(test_frequencies), u.Hz)
        self.n_fft = n_fft
        self.weight_data = weight_data
        self.alpha = alpha

    def transform_data(self):
        # average processed data across epochs including frequency average
        if self.n_fft is None:
            self.n_fft = self.input_node.data.shape[0]
        # we will use whatever weights are present in input_process
        weights = self.input_node.w

        # we compute weights if required
        if self.weight_data and weights is None:
            # run weighted average to provide weights
            _, weights, _, _, _, _ = \
                et_mean(epochs=self.input_node.data,
                        block_size=max(self.block_size, 5),
                        samples_distance=int(max(self.input_node.data.shape[0] // self.n_tracked_points, 10)),
                        weighted=True
                        )

        amp, plv, z, z_critic, p_values, angles, dof, rn = phase_locking_value(
            self.input_node.data,
            weights=weights,
            alpha=self.alpha,
            eps=np.finfo(float).eps * self.input_node.data.unit ** 2.0)
        self.output_node.data = plv
        self.output_node.n_fft = self.n_fft
        self.output_node.domain = Domain.frequency
        self.output_node.statistical_tests = self.get_plv_tests_as_pandas(
            plv=plv,
            angles=angles,
            z=z,
            z_critic=z_critic,
            p_values=p_values,
            dof=dof,
            rn=rn)
        self.output_node.peak_frequency = self.get_plv_peaks_as_pandas(plv=plv,
                                                                       p_values=p_values,
                                                                       angles=angles)

    def get_plv_peaks_as_pandas(self, plv: np.array = None,
                                p_values: np.array = None,
                                angles: np.array = None):
        f_peaks = []
        freq_position = self.output_node.x_to_samples(self.test_frequencies)
        for _i, _ch in enumerate(self.input_node.layout):
            for _fpos, _f in zip(freq_position, self.test_frequencies):
                _f_peak = EegPeak(channel=_ch.label,
                                  x=_f,
                                  amp=plv[_fpos, _i],
                                  significant=bool(p_values[_fpos, _i] < self.alpha),
                                  peak_label="{:10.1f}".format(_f),
                                  show_label=True,
                                  positive=True,
                                  domain=Domain.frequency,
                                  spectral_phase=angles[_fpos, _i])
                f_peaks.append(_f_peak.__dict__)
        _data_pd = pd.DataFrame(f_peaks)
        return _data_pd

    def get_plv_tests_as_pandas(self, plv: np.array,
                                angles: np.array = None,
                                z: float = None,
                                z_critic: float = None,
                                p_values: np.array = None,
                                dof: float = None,
                                rn: float = None):
        r_tests = []
        freq_position = self.output_node.x_to_samples(self.test_frequencies)
        for _i, _ch in enumerate(self.input_node.layout):
            for _fpos, _f in zip(freq_position, self.test_frequencies):
                _test = PhaseLockingValueTest(p_value=p_values[_fpos, _i],
                                              mean_phase=angles[_fpos, _i],
                                              plv=plv[_fpos, _i],
                                              z_critic=z_critic[0, _i],
                                              z_value=z[_fpos, _i],
                                              df_1=dof[0, _i],
                                              channel=_ch.label,
                                              frequency_tested=_f,
                                              rn=rn[_fpos, _i])
                r_tests.append(_test.__dict__)
        _data_pd = pd.DataFrame(r_tests)
        return _data_pd


class CreateAndApplyICAFilter(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 sf_components=np.array([]),
                 sf_thr=0.8,
                 components_to_plot: np.array = np.arange(0, 10),
                 plot_x_lim: [float, float] = None,
                 plot_y_lim: [float, float] = None,
                 n_tracked_points=256,
                 block_size=5,
                 weight_data=True,
                 user_naming_rule: str = '',
                 fig_format: str = '.png',
                 return_figures: bool = False,
                 **kwargs):
        """
        This class will create and apply an ICA filter to the data.
        :param input_process: an InputOutputProcess Class
        :param sf_components: numpy array of integers with indexes of components to be kept
        :param sf_thr: float indicating the percentage of explained variance by components to be kept (this is used when
        sf_components is empty)
        :param components_to_plot: numpy array indicating which components will be plotted.
        :param plot_x_lim: list with minimum and maximum horizontal range of x axis
        :param plot_y_lim: list with minimum and maximum vertical range of y axis
        :param user_naming_rule: string indicating a user naming to be included in the figure file name
        :param fig_format: string indicating the format of the output figure (e.g. '.png' or '.pdf')
        :param return_figures: if True, figures handles will be returnted on self.figures
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(CreateAndApplyICAFilter, self).__init__(input_process=input_process, **kwargs)
        self.sf_components = sf_components
        self.sf_thr = sf_thr
        self.components_to_plot = components_to_plot
        self.plot_x_lim = plot_x_lim
        self.plot_y_lim = plot_y_lim
        self.user_naming_rule = user_naming_rule
        self.fig_format = fig_format
        self.return_figures = return_figures
        self.n_tracked_points = n_tracked_points
        self.block_size = block_size
        self.weight_data = weight_data
        self._unmixing = None
        self._mixing: np.array = None
        self._pwr: np.array = None
        self._components: np.array = None
        self._whitening_m: np.array = None
        self.__kwargs = kwargs

    def transform_data(self):
        spatial_filter = SpatialFilterICA(input_process=self.input_process)
        spatial_filter.run()
        print('applying ICA filter')
        filtered_data = ApplySpatialFilterICA(input_process=spatial_filter,
                                              sf_components=self.sf_components,
                                              sf_thr=self.sf_thr,
                                              **self.__kwargs)
        filtered_data.run()
        self.output_node = filtered_data.output_node

        if self.components_to_plot is not None:
            plotter = PlotSpatialFilterComponents(spatial_filter,
                                                  plot_x_lim=self.plot_x_lim,
                                                  plot_y_lim=self.plot_y_lim,
                                                  user_naming_rule=self.user_naming_rule,
                                                  fig_format=self.fig_format,
                                                  components_to_plot=self.components_to_plot
                                                  )
            plotter.run()

            plotter = ProjectSpatialComponents(spatial_filter,
                                               user_naming_rule=self.user_naming_rule,
                                               plot_x_lim=self.plot_x_lim,
                                               plot_y_lim=self.plot_y_lim,
                                               components_to_plot=self.components_to_plot
                                               )
            plotter.run()


class SpatialFilterICA(InputOutputProcess):
    def __init__(self, input_process=InputOutputProcess,
                 **kwargs):
        """
        This class will create an spatial filter based on the data.
        :param input_process:  InputOutputProcess Class
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(SpatialFilterICA, self).__init__(input_process=input_process, **kwargs)
        self.mixing = None
        self.unmixing = None
        self.pwr = None
        self.whitening_m = None
        self.epoch_size = None

    def transform_data(self):
        # compute spatial filter
        _data = self.input_node.data
        self.epoch_size = _data.shape[0]
        if self.input_node.data.ndim == 3:
            _data = et_unfold(self.input_node.data)
        components, unmixing, mixing, pwr, whitening_m = et_ica_epochs(data=_data,
                                                                       tol=1e-4,
                                                                       iterations=10)
        self.mixing = mixing
        self.unmixing = unmixing
        self.pwr = pwr
        self.whitening_m = whitening_m
        self.output_node.data = et_fold(components, epoch_size=self.epoch_size)


class ApplySpatialFilterICA(InputOutputProcess):
    def __init__(self, input_process: SpatialFilterICA = None,
                 sf_components: np.array = np.array([]),
                 sf_thr: float = 0.8,
                 **kwargs):
        """
        This class will apply an SpatialFilter to the data. If sf_components are paased, only those will be used to
        filter the data
        :param input_process: an SpatialFilter InputOutputProcess Class
        :param sf_components: numpy array of integers with indexes of components to be kept
        :param sf_thr: float indicating the percentage of explained variance by components to be kept (this is used when
        sf_components is empty)
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(ApplySpatialFilterICA, self).__init__(input_process=input_process, **kwargs)
        self.sf_components = sf_components
        self.sf_thr = sf_thr

    def transform_data(self):
        print('applying spatial filter')
        if self.sf_components is None or not self.sf_components.size:
            cumpower = np.cumsum(self.input_process.pwr) / np.sum(self.input_process.pwr)
            n_idxs = np.argwhere(cumpower <= self.sf_thr)
            n_components = np.arange(n_idxs.size)
        else:
            n_components = self.sf_components

        clean_epochs = self.mix_components(components_idx=n_components)
        if self.input_process.epoch_size is not None:
            clean_epochs = et_fold(clean_epochs, epoch_size=self.input_process.epoch_size)
        self.output_node.data = clean_epochs

    def mix_components(self,
                       components_idx: np.array = None):
        w_c = np.zeros(self.input_process.mixing.shape)
        w_c[:, components_idx] = 1
        s_clean = self.input_process.mixing * w_c
        _components = et_unfold(self.input_process.output_node.data)
        clean_data = s_clean.dot(_components.T).T
        return clean_data


class Covariance(InputOutputProcess):
    def __init__(self, input_process: InputOutputProcess = None,
                 normalized=True,
                 return_figures: bool = False,
                 ini_time: u.Quantity = 0 * u.s,
                 end_time: u.Quantity = np.Inf * u.s,
                 fig_format: str = '.png',
                 fontsize: float = 12,
                 user_naming_rule: str = '',
                 save_to_file: bool = True,
                 **kwargs):
        """
        This class will apply compute the covariance matrix
        :param input_process: an SpatialFilter InputOutputProcess Class
        :param normalized: if true, covariance will be normalized (i.e. correlation)
        :param return_figures: If true, handle to figure will be passed to self.figures
        :param ini_time: float indicating the starting time to compute covariance
        :param end_time: float indicating the ending time to compute covariance
        :param fig_format: string indicating the format of the output figure (e.g. '.png' or '.pdf')
        :param fontsize: size of fonts in plot
        :param idx_channels_to_plot: np.array indicating the index of the channels to plot
        :param user_naming_rule: string indicating a user naming to be included in the figure file name
        :param kwargs: extra parameters to be passed to the superclass
        """
        super(Covariance, self).__init__(input_process=input_process, **kwargs)
        self.normalized = normalized
        self.ini_time = set_default_unit(ini_time, u.s)
        self.end_time = set_default_unit(end_time, u.s)
        self.fig_format = fig_format
        self.fontsize = fontsize
        self.user_naming_rule = user_naming_rule
        self.return_figures = return_figures
        self.save_to_file = save_to_file
        self.figures = None

    def transform_data(self):
        data = self.input_node.data.value
        _ini_time = np.minimum(np.maximum(0,
                               self.input_node.x_to_samples(np.array([self.ini_time.to(u.s).value]) * u.s).squeeze()),
                               data.shape[0]).astype(int)
        _end_time = np.maximum(0,
                               np.minimum(
                                   data.shape[0],
                                   self.input_node.x_to_samples(np.array([self.end_time.to(u.s).value]) * u.s).squeeze()
                               )).astype(int)
        data = self.input_node.data.value[_ini_time: _end_time, :]
        if self.normalized:
            cov = np.corrcoef(data.T)
            v_min = -1 * u.dimensionless_unscaled
            v_max = 1 * u.dimensionless_unscaled
        else:
            cov = np.cov(data.T.value)
            v_min = np.min(cov)
            v_max = np.max(cov)

        self.output_node.data = cov
        # generate table
        table = pd.DataFrame()
        channels = [_ch.label for _ch in self.input_node.layout]
        for _i in range(cov.shape[0]):
            ch_i = channels[_i]
            for _j in range(cov.shape[1]):
                ch_j = channels[_j]
                table = pd.concat([table, pd.DataFrame([{'channel_x': ch_i,
                                                         'channel_y': ch_j,
                                                         'covariance': cov[_i, _j],
                                                         'normalized:': self.normalized}])])
        self.output_node.statistical_tests = table
        fig = plt.figure()
        ax = fig.add_subplot()
        maxis = ax.matshow(cov, vmin=v_min.value, vmax=v_max.value)
        ax.set_xticks(np.arange(0, len(channels)))
        ax.set_yticks(np.arange(0, len(channels)))
        ax.set_xticklabels(channels)
        ax.set_yticklabels(channels)
        plt.colorbar(maxis)
        mask = np.ones(cov.shape, dtype=bool)
        np.fill_diagonal(mask, 0)
        max_value = cov[mask].max()
        min_value = cov[mask].min()
        print('Maximum / Minimum covariance: {:} / {:}'.format(max_value, min_value))
        if self.save_to_file:
            figure_dir_path = self.input_node.paths.figures_current_dir
            figure_basename = self.input_process.__class__.__name__ + '_' + self.user_naming_rule
            fig.savefig(figure_dir_path + figure_basename + 'covariance' + self.fig_format)
        if self.return_figures:
            self.figures = fig
        else:
            plt.close(fig)
            gc.collect()
