import copy
import itertools
import abc
import matplotlib.pyplot as plt
import numpy as np
import datetime
from pyeeg.definitions.channel_definitions import Domain, ChannelItem, ChannelType
from pyeeg.processing.tools.multiprocessing.multiprocessesing_filter import filt_data
from pyeeg.tools.signal_generator import noise_functions as nf
from pyeeg.io.eeg.reader import eeg_reader
from pyeeg.layouts import layouts
import astropy.units as u
from pyeeg.directories.tools import DirectoryPaths
from pyeeg.processing.events.event_tools import detect_events, get_event_times
import inspect
import pandas as pd
import io
from pyeeg.definitions.events import Events
from pathlib import Path
from os.path import sep
from pyeeg.processing.tools.template_generator.auditory_waveforms import eog, fade_in_out_window
from pyeeg.tools.units.unit_tools import set_default_unit
from pyeeg.definitions.events import SingleEvent
import gc
from scipy.linalg import eigh, cholesky
import pyqtgraph as pg
pg.setConfigOption('leftButtonPan', False)


class DataNode(object):
    """
    Core data object used to keep data. During the creation, it requires the sampling rate and the actual data.
    Data is passed in a numpy array of n * m or n * m * t, where n represent samples, m channels, and t trials.
    Units are passed via unit kargs (default units is microVolts)
    """
    def __init__(self,
                 fs=None,
                 data: np.array = np.array([]),
                 domain=Domain.time,
                 x_offset=0.0 * u.s,
                 n_fft=None,
                 layout: np.array([ChannelItem]) = None,
                 process_history=[],
                 events: Events = None,
                 events_annotations: () = None,
                 paths: DirectoryPaths = None,
                 rn: np.array = None,
                 rn_df: np.array = None,
                 cum_rn: np.array = None,
                 snr: np.array = None,  # it may content snr for different ROIs
                 cum_snr: np.array = None,
                 s_var=None,
                 alpha=0.05,
                 n: int = None,
                 w: np.array = None,
                 statistical_tests=None,
                 peak_times=None,
                 peak_frequency=None,
                 peak_to_peak_amplitudes=None,
                 peak_time_windows=None,
                 roi_windows=None,
                 markers=None
                 ):
        # n * m * t data array
        # if data represent time or frequency
        # time offset from origin
        # layout
        # list used to store processing history that resulted in the current data

        self.fs = fs
        self._data = data
        self.domain = domain
        self.x_offset = x_offset
        self.layout = layout
        self.events = events
        self.events_annotations = events_annotations
        self.process_history = process_history
        self.paths = paths
        # signal statistics
        self.rn = rn
        self.cum_rn = cum_rn
        self.snr = snr
        self.cum_snr = cum_snr
        self.s_var = s_var
        self.n = n
        self.rn_df = rn_df
        self.w = w
        self.alpha = alpha
        self.statistical_tests = statistical_tests
        # peak measures
        self.peak_times = peak_times
        self.peak_frequency = peak_frequency
        self.peak_to_peak_amplitudes = peak_to_peak_amplitudes
        self.peak_time_windows = peak_time_windows
        self.roi_windows = roi_windows
        self.markers = markers
        self.n_fft = n_fft
        if self.layout is None and data.size:
            self.layout = np.array([ChannelItem() for _ in range(self.data.shape[1])])  # default channel information
        self._x = None
        self._y = None

    def get_x(self):
        if self.domain == Domain.time:
            out = np.arange(self.data.shape[0]) / self.fs - self.x_offset
            out = out.to(u.s)
        if self.domain == Domain.frequency:
            out = np.arange(self.data.shape[0]) * self.fs / self.n_fft
            out = out.to(u.Hz)
        if self.domain == Domain.time_frequency:
            out = self._x
        return out

    def set_x(self, value):
        self._x = value
    x = property(get_x, set_x)

    def get_y(self):
        return self._y

    def set_y(self, value):
        self._y = value

    y = property(get_y, set_y)

    def apply_layout(self, layout=None):
        for _s_i in layout.layout:
            for _i, _l in enumerate(self.layout):
                if _l.idx == _s_i.idx:
                    self.layout[_i] = _s_i
                    break

    def delete_channel_by_idx(self, idx: np.array = np.array([])):
        labels = '/'.join([_ch.label for _ch in self.layout[idx]])
        self.data = np.delete(self.data, idx, 1)
        self.layout = np.delete(self.layout, idx)
        print('removed channels: ' + ''.join(labels))

    def delete_channel_by_label(self, label=''):
        _idx = np.array([_i for _i, _lay in enumerate(self.layout) if _lay.label == label])
        self.delete_channel_by_idx(idx=_idx)

    def get_channel_idx_by_label(self, labels: np.array([str]) = None):
        if labels is None:
            labels = np.array([_channel.label for _channel in self.layout])
        if isinstance(labels, list):
            labels = np.array(labels)
        # ensure unique labels and remove repeated in same order
        _, idx_labels = np.unique(labels, return_index=True)
        labels = labels[np.sort(idx_labels)]
        all_labels = np.array([_channel.label for _idx, _channel in enumerate(self.layout)])
        idx = []
        for _label in labels:
            _idx = np.argwhere(_label == all_labels)
            if _idx.size:
                idx.append(_idx.squeeze())
        idx = np.array(idx)
        return idx

    def get_channel_idx_by_type(self, channel_type=ChannelType.Event):
        all_idx = np.array([_idx for _idx, _channel in enumerate(self.layout) if _channel.type == channel_type])
        idx = np.unique(all_idx)
        return idx

    def get_channel_label_by_idx(self, idx: np.array = np.array([])) -> [str]:
        if idx.size == 0:
            idx = np.arange(self.layout.size)
        labels = [_ch.label for _ch in self.layout[idx]]
        return labels

    def set_data(self, value=np.array):
        self._data = value

    def get_data(self):
        return self._data
    data = property(get_data, set_data)

    def get_max_snr_per_channel(self):
        if self.snr is not None:
            max_snr = np.nanmax(np.atleast_2d(self.snr), axis=0)
        else:
            max_snr = np.array([None] * self._data.shape[1])
        return np.atleast_1d(np.squeeze(max_snr))

    def get_max_s_var_per_channel(self):
        max_var = np.nanmax(self.s_var, axis=0)
        return np.atleast_1d(np.squeeze(max_var))

    def x_to_samples(self, value: np.array) -> np.array:
        out = np.array([])
        for _v in value:
            _value = _v.to(self.x.unit).value
            if np.isinf(_value):
                out = np.append(out, _value)
            else:
                out = np.append(out, np.argmin(np.abs(self.x.value - np.array(_value)))).astype(np.int)
        return out

    def data_to_pandas(self):
        _row_test = []
        for i in range(self._data.shape[1]):
            data_binary_x = io.BytesIO()
            np.save(data_binary_x, self.x.value)
            data_binary_y = io.BytesIO()
            np.save(data_binary_y, self.data[:, i].value)
            data_binary_x.seek(0)
            data_binary_y.seek(0)
            _row_time_data = {'domain': self.domain,
                              'channel': self.layout[i].label,
                              'x': data_binary_x.read(),
                              'y': data_binary_y.read(),
                              'x_unit': self.x.unit.to_string(),
                              'y_unit': self.data[:, i].unit.to_string(),
                              'snr': self.get_max_snr_per_channel()[i]}
            _row_test.append(_row_time_data)
        _data_pd = pd.DataFrame(_row_test)
        return _data_pd

    def append_new_channel(self, new_data: np.array = None, layout_label: str = ''):
        new_channel = ChannelItem(label=layout_label)
        self.layout = np.append(self.layout, new_channel)
        self.data = np.append(self.data.value, new_data.to_value(self.data.unit), axis=1) * self.data.unit
        self.rn = np.append(self.rn, np.nan)
        if self.snr is not None:
            for _i, _s in enumerate(self.snr):
                _s = np.append(_s, np.nan)
                self.snr[_i] = _s
        if self.s_var is not None:
            for _i, _s in enumerate(self.s_var):
                _s = np.append(_s, np.nan)
                self.s_var[_i] = _s

    def clip(self, ini_time: u.Quantity = 0 * u.s, end_time: u.Quantity = np.inf * u.s):
        ini_time = set_default_unit(ini_time, u.s)
        end_time = set_default_unit(end_time, u.s)
        if np.isinf(end_time):
            end_time = self.x[-1]
        samples = self.x_to_samples(np.array([ini_time.to(u.s).value,
                                              end_time.to(u.s).value]) * u.s)
        _extact_time_offset = self.x[samples[0]]
        self._data = self._data[samples[0]: samples[-1], ::]
        self.events.clip(ini_time=ini_time, end_time=end_time)
        # apply offset
        self.events.set_offset(time_offset=_extact_time_offset)


class InputOutputProcess(metaclass=abc.ABCMeta):
    """
    Core abstract input-output function. Each inherit class must provide an input data, a process function and and
    output data set
    """
    def __init__(self, input_process=object,
                 keep_input_node=True,
                 **kwargs):
        self.input_process: InputOutputProcess = input_process  # Input data must be InputOutputProcess class
        self.function_args: dict = kwargs
        self.input_node: DataNode = None
        self.output_node: DataNode = None
        self.process_parameters: dict = kwargs  # dict with all the parameters used in process function
        self.keep_input_node: bool = keep_input_node
        self.figures: [plt.Figure] = None
        self.name: str = None
        self.__html__figure_exporter = None

    def transform_data(self):
        return 'core method to generate output data'

    def run(self):
        self.input_node = self.input_process.output_node
        callable_list = [_i for _i in dir(DataNode) if callable(getattr(DataNode, _i))]
        properties_list = [_i[0] for _i in inspect.getmembers(DataNode, lambda o: isinstance(o, property))]
        attributes = [a for a in dir(self.input_node) if not (a.startswith('__') or a.startswith('_')) and
                      a not in callable_list and a not in properties_list]
        pars = {_a: getattr(self.input_node, _a) for _a in attributes}
        self.output_node = DataNode(data=np.array([]),
                                    **pars
                                    )  # Output data must be DataNode class
        self.transform_data(**self.function_args)

        if not self.keep_input_node:
            if isinstance(self.input_node, DataNode):
                self.input_node.data = None
            self.input_node = None
            gc.collect()
        return

    def plot_input_output(self,
                          plot_input: bool = False,
                          plot_output: bool = True):
        if not plot_input and not plot_output:
            return
        symbols = itertools.cycle(
            ['o', 's', 't', 'd', '+', 't1', 't2', 't3', 'p', 'h', 'star', 'x', 'arrow_up', 'arrow_right',
             'arrow_down', 'arrow_left', 'crosshair'])
        data_in = self.input_node.data
        name_in = self.input_process.name
        name_out = self.name
        data_out = self.output_node.data
        if self.input_node.domain == Domain.frequency:
            data_in = np.abs(data_in)
        if self.output_node.domain == Domain.frequency:
            data_out = np.abs(data_out)

        if data_in is not None or data_out is not None:
            win = pg.GraphicsWindow(title=self.__class__.__name__ + '/' + self.input_process.__class__.__name__)
            ax = win.addPlot(row=1, col=1)
            ax.addLegend()
        else:
            return

        offset_in, offset_out = 0, 0
        channels_in, channels_out = [], []
        if data_in is not None and plot_input:
            offset_in = (np.max(np.abs(data_in.flatten() - np.mean(data_in.flatten()))) +
                         np.min(np.abs(data_in.flatten() - np.mean(data_in.flatten())))) / 2
            channels_in = [_l.label for _l in self.input_node.layout]
        if data_out is not None and plot_output:
            offset_out = (np.max(np.abs(data_out.flatten() - np.mean(data_out.flatten()))) +
                          np.min(np.abs(data_out.flatten() - np.mean(data_out.flatten())))) / 2
            channels_out = [_l.label for _l in self.output_node.layout]

        channels = np.unique(channels_in + channels_out)
        offset = np.maximum(offset_in, offset_out)
        if plot_input and data_in is not None:
            x_in = self.input_node.x
            ax.setDownsampling(auto=True, mode='peak')
            ax.showGrid(True, True)
            _color = (255, 0, 0)
            for _i, _label in enumerate(channels_in):
                _name = name_in if _i == 0 else None
                _pos = np.argwhere(channels == _label).squeeze()
                if not _pos.size:
                    continue
                text_item = pg.TextItem(_label, anchor=(0.0, 0.0))
                text_item.setPos(0, offset.value * _pos)
                if data_in.ndim == 2:
                    ax.plot(x_in, data_in[:, _i] - np.mean(data_in[:, _i]) +
                            offset * _pos, pen=_color, name=_name)

                if data_in.ndim == 3:
                    for _t in range(data_in.shape[2]):
                        ax.plot(x_in, data_in[:, _i, _t] - np.mean(data_in[:, _i, _t]) +
                                offset * _pos, pen=_color, name=_name)
                        ax.addItem(text_item)
                ax.addItem(text_item)

                if self.input_node.events is not None:
                    itertools.tee(symbols)
                    _codes = np.unique(self.input_node.events.get_events_code())
                    for _code in _codes:
                        _events = self.input_node.events.get_events_time(code=_code)
                        ax.plot(x=_events.to_value(_events.unit),
                                y=0.0 * _events.value + offset * _pos,
                                pen=None,
                                symbol=next(symbols),
                                symbolBrush=_color,
                                symbolSize=6)

            if self.input_node.domain == Domain.time:
                ax.setLabel('bottom', "Time [{:}]".format(self.input_node.x.unit))
                ax.setLabel('left', "Amplitude [{:}]".format(self.input_node.data.unit))
            if self.input_node.domain == Domain.frequency:
                ax.setLabel('bottom', "Time [{:}]".format(self.input_node.x.unit))
                ax.setLabel('left', "Frequency [{:}]".format(self.input_node.data.unit))

        if plot_output and data_out is not None:
            _color = (0, 0, 255)
            x_out = self.output_node.x
            ax.setDownsampling(ds=100.0, mode='peak')
            ax.showGrid(True, True)
            for _i, _label in enumerate(channels_out):
                _name = name_out if _i == 0 else None
                _pos = np.argwhere(channels == _label).squeeze()
                if not _pos.size:
                    continue
                text_item = pg.TextItem(_label, anchor=(0.0, 0.0))
                text_item.setPos(0, offset.value * _pos)
                if data_out.ndim == 2:
                    ax.plot(x_out, data_out[:, _i] - np.mean(data_out[:, _i]) +
                            offset * _pos, pen=_color, name=_name)
                if data_out.ndim == 3:
                    for _t in range(data_out.shape[2]):
                        ax.plot(x_out, data_out[:, _i, _t] - np.mean(data_out[:, _i, _t]) +
                                offset * _pos, pen=_color, name=_name)
                ax.addItem(text_item)
                if self.output_node.events is not None:
                    itertools.tee(symbols)
                    _codes = np.unique(self.output_node.events.get_events_code())
                    for _code in _codes:
                        _events = self.output_node.events.get_events_time(code=_code)
                        ax.plot(x=_events,
                                y=0.0 * _events.value + offset * _pos,
                                pen=None,
                                symbol=next(symbols),
                                symbolBrush=_color,
                                symbolSize=6)

            if self.output_node.domain == Domain.time:
                ax.setLabel('bottom', "Time [{:}]".format(self.output_node.x.unit))
                ax.setLabel('left', "Amplitude [{:}]".format(self.output_node.data.unit))
            if self.output_node.domain == Domain.frequency:
                ax.setLabel('bottom', "Time [{:}]".format(self.output_node.x.unit))
                ax.setLabel('left', "Frequency [{:}]".format(self.output_node.data.unit))

        # self._pg.QtGui.QApplication.processEvents()
        pg.QtGui.QApplication.processEvents()

        ex = pg.exporters.SVGExporter(win.scene())
        out = ex.export(toBytes=True)
        self.__html__figure_exporter = out.decode("utf-8")
        return self

    def _repr_html_(self):
        return self.__html__figure_exporter


class ReadInputData(InputOutputProcess):
    def __init__(self,
                 file_path: str = None,
                 channels_idx: np.array = np.array([]),
                 ini_time: u.quantity.Quantity = 0 * u.s,
                 end_time: u.quantity.Quantity = np.Inf * u.s,
                 layout_file_name: str = None,
                 figures_subset_folder: str = '',
                 fs_col_name: str = None,
                 gain_col_name: str = None,
                 fs_unit: u.quantity.Quantity = u.Hz,
                 gain_unit: u.quantity.Quantity = u.uV,
                 gain_inverted: bool = False,
                 event_channel_label: str = None
                 ) -> InputOutputProcess:
        """
        This pipeline class handles reading eeg data files.

        :param file_path: path to file to be read
        :param channels_idx: numpy array indicating specifics channels to be read. If empty, all channels will be read
        :param ini_time: time in seconds from where to read data
        :param end_time: time in seconds up to where to read data
        :param layout_file_name: Name of layout mapping the channel labels to an specific topographic map
        :param figures_subset_folder: string used to generate a sub-folder within the main figure path. Useful to
        generate specific folder paths in a dinamy way.
        :param fs_col_name: provide the name of the column containing the sampling rate (used when data comes in .csv
        files)
        :param gain_col_name: provide the name of the column containing the gain to scale the data (used when data comes
         in .csv files)
        :param fs_unit: unit of sampling rate
        :param gain_unit: unit of gain
        :param gain_inverted: indicates whether the data will be multiplied or divided by the gain.
        :param event_channel_label: event_channel_label: string indicating the label of the channel containing all
         events. This is usually the 'Status' channel for bdf files and 'EDF Annotations' for EDF files, however, some
        devices provide other labels, e.g. 'BDF Annotations', therefore we leve this open
        """
        super(ReadInputData, self).__init__()
        self.reader = eeg_reader(file_path,
                                 fs_col_name=fs_col_name,
                                 gain_col_name=gain_col_name,
                                 fs_unit=fs_unit,
                                 gain_unit=gain_unit,
                                 gain_inverted=gain_inverted,
                                 event_channel_label=event_channel_label)
        self.file_path = file_path
        self.channels_idx = channels_idx
        self.ini_time = ini_time
        self.end_time = end_time
        self.output_node = None
        self.layout_file_name = layout_file_name
        self.figures_subset_folder = figures_subset_folder
        self.input_node = DataNode(fs=self.reader.fs,
                                   domain=Domain.time,
                                   layout=self.reader.default_layout,
                                   paths=DirectoryPaths(file_path=self.reader.file_name,
                                                        delete_all=False, delete_figures=False,
                                                        figures_subset_folder=figures_subset_folder)
                                   )

    def run(self):
        data, events, units, annotations = self.reader.get_data(channels_idx=self.channels_idx,
                                                                ini_time=self.ini_time,
                                                                end_time=self.end_time)

        # if data.shape[1] > self.channels_idx.size:
        #     self.events_idx = data.shape[1] - 1
        self.output_node = DataNode(data=data,
                                    fs=self.reader.fs,
                                    domain=Domain.time,
                                    layout=self.reader.default_layout,
                                    )
        if self.layout_file_name is not None:
            self.output_node.apply_layout(layouts.Layout(file_name=self.layout_file_name))

        self.output_node.paths = self.input_node.paths
        self.output_node.events = self.get_events(events)
        self.output_node.events_annotations = annotations
        if annotations[0] is not None and annotations[1] is not None:
            to_print = pd.DataFrame.from_dict(
                {'Annotation': annotations[0],
                 'Code': annotations[1]})
            print(to_print.to_string())

    def get_events(self, events):
        _events = get_event_times(event_channel=events, fs=self.output_node.fs)
        return _events


class MergeMultipleFiles(InputOutputProcess):
    def __init__(self,
                 file_paths: [str] = None,
                 channels_idx: np.array = np.array([]),
                 layout_file_name: str = None,
                 figures_subset_folder: str = '',
                 fs_col_name: str = None,
                 gain_col_name: str = None,
                 fs_unit: u.quantity.Quantity = u.Hz,
                 gain_unit: u.quantity.Quantity = u.uV,
                 gain_inverted: bool = False
                 ) -> InputOutputProcess:
        super(MergeMultipleFiles, self).__init__()
        self.readers = np.array([])
        for _file in file_paths:
            _reader = eeg_reader(_file,
                                 fs_col_name=fs_col_name,
                                 gain_col_name=gain_col_name,
                                 fs_unit=fs_unit,
                                 gain_unit=gain_unit,
                                 gain_inverted=gain_inverted)
            self.readers = np.append(self.readers, _reader)
        self.file_paths = file_paths
        self.channels_idx = channels_idx
        self.ini_time = 0 * u.s
        self.end_time = np.Inf * u.s
        self.output_node = None
        self.layout_file_name = layout_file_name
        self.figures_subset_folder = figures_subset_folder
        self.input_node = DataNode(fs=self.readers[0].fs,
                                   domain=Domain.time,
                                   layout=self.readers[0].default_layout,
                                   paths=DirectoryPaths(file_path=self.readers[0].file_name,
                                                        delete_all=False, delete_figures=False,
                                                        figures_subset_folder=figures_subset_folder)
                                   )

    def run(self):
        all_data = None
        all_events = None
        # sort readers by time
        _date_format = "%d.%m.%y/%H.%M.%S"
        dates = np.array([_reader._header['start_date'] + '/' + _reader._header['start_time'] for
                          _reader in self.readers])
        sorted_idx = np.argsort([datetime.datetime.strptime(_date, _date_format) for _date in dates])
        print(dates[sorted_idx])
        for _reader in self.readers[sorted_idx]:
            data, events, units, annotations = _reader.get_data(channels_idx=self.channels_idx,
                                                                ini_time=self.ini_time,
                                                                end_time=self.end_time)

            # demean data
            data = data - np.mean(data, axis=0)
            if all_data is not None:
                all_data = np.concatenate((all_data, data))
                all_events = np.concatenate((all_events, events))
            else:
                all_data = data
                all_events = events

        self.output_node = DataNode(data=all_data,
                                    fs=self.readers[0].fs,
                                    domain=Domain.time,
                                    layout=self.readers[0].default_layout,
                                    )
        if self.layout_file_name is not None:
            self.output_node.apply_layout(layouts.Layout(file_name=self.layout_file_name))

        self.output_node.paths = self.input_node.paths
        self.get_events(all_events)

    def get_events(self, events):
        events = detect_events(event_channel=events, fs=self.output_node.fs)
        events = Events(events=np.array(events))
        for i, _code in enumerate(np.unique(events.get_events_code())):
            print('Event code:', _code, 'Number of events:', events.get_events_code(code=_code).size)
        self.output_node.events = events


def correlated_noise(method: str = 'cholesky',
                     noise_token: np.array = None,
                     n_channels: int = 4,
                     covariance_matrix: np.array = None,
                     ):
    assert covariance_matrix.shape == (n_channels, n_channels)
    # We need a matrix `c` for which `c*c^T = r`.  We can use, for example,
    # the Cholesky decomposition, or the we can construct `c` from the
    # eigenvectors and eigenvalues.
    if method == 'cholesky':
        # Compute the Cholesky decomposition.
        c = cholesky(covariance_matrix, lower=True)
    else:
        # Compute the eigenvalues and eigenvectors.
        evals, evecs = eigh(covariance_matrix)
        # Construct c, so c*c^T = r.
        c = np.dot(evecs, np.diag(np.sqrt(evals)))

    # Convert the data to correlated random variables.
    y = np.dot(c, noise_token.T).T
    return y


class GenerateInputData(InputOutputProcess):
    def __init__(self,
                 template_waveform: np.array = None,
                 stimulus_waveform: np.array = None,
                 fs: u.quantity.Quantity = 16384.0 * u.Hz,
                 alternating_polarity=False,
                 n_channels: int = None,
                 snr: float = 3,
                 line_noise_amplitude: u.quantity.Quantity = 5 * u.uV,
                 include_non_stationary_noise_events=False,
                 noise_events_interval: u.quantity.Quantity = 25.0 * u.s,
                 noise_events_duration: u.quantity.Quantity = 15.0 * u.s,
                 noise_events_power_delta_db: float = 6.0,
                 noise_seed: float = None,
                 noise_attenuation: float = 0,
                 noise_covariance_matrix: np.array = None,
                 mixing_matrix: np.array = None,
                 include_eog_events: bool = False,
                 fade_in_out: bool = False,
                 event_times: np.array = None,
                 event_code: float = 1.0,
                 layout_file_name: str = None,
                 figures_path: str = None,
                 figures_subset_folder: str = '',
                 return_noise_only=False) -> InputOutputProcess:
        """
        This class allows to generate data that can be used without the need of having a bdf or edf file.
        This InputOutput process takes an input template and convolve it with delta at event times.
        The output will have as many channels as desired and the maximum snr will be given by snr.
        If requested, non-stationary noise events will be generated. The snr will be calculated relative to the
        stationary noise. The non-stationary events will vary around that stationary level by x dB as passed in
        noise_events_power_delta_db.
        The output events will be coded with the provided event code.
        :param template_waveform: a numpy array with the template waveform to be use as signal
        :param stimulus_waveform: a numpy array with the presented waveform. Useful to generate stimulus artifacts
        :param fs: the sampling rate of the template_waveform
        :param alternating_polarity: if true, stimulus_waveform is alternating in polarity for every epoch
        :param n_channels: number of channels to generate
        :param snr: signal-to-noise ratio defined as variance_signal / variance_noise, 0 < snr < inf.
        This will be used to set the amplitude of the stationary noise
        :param line_noise_amplitude: amplitude in uV of 50 Hz line noise
        :param include_non_stationary_noise_events: if True, non-stationary noise events will be included
        :param noise_events_interval: interval time [in seconds] between non-stationary events
        :param noise_events_duration: maximum duration of each non-stationary noise event
        :param noise_events_power_delta_db: the dB change of power in the noise noise relative to stationary noise power
        :param noise_seed: float to set the random generator
        :param noise_attenuation: indicates the attenuation of the noise (1 / f ** noise_attenuation), set to 0 for
        white noise and to 3 for pink noise
        :param noise_covariance_matrix: n_channels by n_channels matrix indicating the desired covariance of the noise
        :param mixing_matrix: numpy array with the coefficients that will be applied to template wavefrom
        :param include_eog_events: if True, eog-like artifacts will be included
        :param fade_in_out: It True, convolved template will be faded in and out to prevent onset effects
        :param event_times: numpy array with the location of the events. The wave
        :param event_code: desired event code to be assigned to time events
        :param layout_file_name: name of layout to be used. e.g. "biosemi32.lay"
        :param figures_path: path to save generated figures
        :param figures_subset_folder: string indicating a subfolder name in figures_path
        :param return_noise_only: boolean indicating if the output will consist of noise only or not. This is useful to
        compare results with and without a target signal whilst keeping the noise output fixed.
        """
        super(GenerateInputData, self).__init__()
        self.template_waveform = template_waveform
        self.stimulus_waveform = stimulus_waveform
        self.alternating_polarity = alternating_polarity
        self.n_channels = n_channels
        self.snr = snr
        self.line_noise_amplitude = set_default_unit(line_noise_amplitude, u.V)
        self.fs = set_default_unit(fs, u.Hz)
        self.include_non_stationary_noise = include_non_stationary_noise_events
        self.noise_events_interval = set_default_unit(noise_events_interval, u.s)
        self.noise_events_duration = set_default_unit(noise_events_duration, u.s)
        self.noise_events_power_delta_db = noise_events_power_delta_db
        self.noise_seed = noise_seed
        self.noise_attenuation = noise_attenuation
        self.noise_covariance_matrix = set_default_unit(noise_covariance_matrix, template_waveform.unit ** 2.0)
        self.event_times = set_default_unit(event_times, u.s)
        self.event_code = event_code
        self.include_eog_events = include_eog_events
        self.simulated_artifact = None
        self.simulated_neural_response = None
        self.simulated_artifact_response = None
        self.output_node = None
        self.layout_file_name = layout_file_name
        self.mixing_matrix = mixing_matrix
        self.fade_in_out = fade_in_out
        self.return_noise_only = return_noise_only
        figures_path = figures_path if figures_path is not None else str(Path.home()) + '{:}'.format(sep +
                                                                                                     'pyeeg_python' +
                                                                                                     sep +
                                                                                                     'test' +
                                                                                                     sep +
                                                                                                     'figures')
        self.figures_subset_folder = figures_subset_folder

        _ch = []
        [_ch.append(ChannelItem(label='CH_{:}'.format(i), idx=i)) for i in range(n_channels)]
        layout = np.array(_ch)
        self.input_node = DataNode(fs=fs,
                                   domain=Domain.time,
                                   layout=layout,
                                   paths=DirectoryPaths(file_path=figures_path,
                                                        delete_all=False,
                                                        delete_figures=False,
                                                        figures_subset_folder=figures_subset_folder)
                                   )

    def run(self):
        if self.noise_seed is not None:
            np.random.seed(self.noise_seed)
        # create synthetic data
        _events_idx = np.floor(self.event_times * self.fs).astype(np.int)
        events = np.zeros((int(self.event_times[-1] * self.fs) + 1, 1))
        events[_events_idx, :] = 1
        source = self.template_waveform
        # convolve source with dirac train to generate entire signal
        source = filt_data(input_data=source, b=events.flatten(), mode='full', onset_padding=False)
        fade_window = np.ones(source.shape)
        if self.fade_in_out:
            fade_window = fade_in_out_window(n_samples=source.shape[0],
                                             fraction=10 * self.template_waveform.shape[0]/source.shape[0])
        source = source * fade_window

        artifacts = np.zeros(source.shape)
        if self.stimulus_waveform is not None:
            _events_source = events.copy()
            if self.alternating_polarity:
                _events_source[_events_idx[1:-1:2], :] = -1
            artifacts = filt_data(input_data=self.stimulus_waveform, b=_events_source.flatten(), mode='full',
                                  onset_padding=False)
        self.simulated_artifact = artifacts
        # mixing source matrix
        if self.mixing_matrix is None:
            self.mixing_matrix = np.mod(
                np.arange(self.n_channels) + 1, self.n_channels // 2 + 1) * 1 / (self.n_channels / 2)
            self.mixing_matrix = np.expand_dims(self.mixing_matrix, 0)
        # mixing artifact matrix
        art_coeff = np.roll(self.mixing_matrix, self.mixing_matrix.size // 4)

        # generates and convolve eye artifacts
        eog_waveform = np.zeros(source.shape) * u.uV
        if self.include_eog_events:
            oeg_events = np.zeros(source.shape)
            eog_time_events = np.sort(np.random.randint(0, source.shape[0],
                                                        int((source.shape[0] / self.fs) // (4 * u.s))) / self.fs)
            _oeg_events_idx = np.floor(eog_time_events * self.fs).astype(np.int)
            oeg_events[_oeg_events_idx, :] = 1
            _oeg_template, _ = eog(self.fs)
            oeg_events = filt_data(input_data=_oeg_template,
                                   b=oeg_events.flatten(),
                                   mode='full',
                                   onset_padding=False)
            eog_waveform = oeg_events[0: source.shape[0]]

        neural_response = source * self.mixing_matrix
        artifact_response = artifacts * art_coeff
        self.simulated_neural_response = neural_response
        self.simulated_artifact_response = artifact_response
        s = neural_response + artifact_response
        eog_artifacts = eog_waveform * self.mixing_matrix / 2
        # generate noise
        noise = nf.generate_modulated_noise(fs=self.fs.value,
                                            f_noise_low=0,
                                            f_noise_high=self.fs.value / 2,
                                            duration=s.shape[0] / self.fs.value,
                                            n_channels=self.n_channels,
                                            attenuation=self.noise_attenuation,
                                            noise_seed=self.noise_seed)
        if self.noise_covariance_matrix is None:
            self.noise_covariance_matrix = np.zeros((self.n_channels, self.n_channels))
            self.noise_covariance_matrix[np.diag_indices_from(self.noise_covariance_matrix)] = 1

        noise = correlated_noise(noise_token=noise,
                                 n_channels=self.n_channels,
                                 covariance_matrix=self.noise_covariance_matrix)

        print('noise correlation: {:}'.format(np.corrcoef(noise.T)))
        noise_var = np.var(noise, axis=0)
        if self.include_non_stationary_noise:
            # non-stationary noise (e.g. 1 event every 20 seconds)
            _new_noise_events = np.arange(0, source.shape[0],
                                          self.noise_events_interval * self.fs) / self.fs
            for _i, _ne in enumerate(_new_noise_events):
                _ini_pos = int(_ne * self.fs)
                _noise_samples = np.random.randint(1, int(self.fs * self.noise_events_duration))
                _ini_pos = np.minimum(_ini_pos, source.shape[0])
                _end_pos = np.minimum(_ini_pos + _noise_samples - 1, source.shape[0])
                noise[_ini_pos:_end_pos, :] += np.random.normal(0, 10 ** (self.noise_events_power_delta_db / 20) * 0.1,
                                                                size=(_end_pos - _ini_pos, self.n_channels))

        # generate line noise
        line_coeff = (1 + np.random.rand(1, self.mixing_matrix.size) * 0.0001) * self.line_noise_amplitude
        _line_signal = np.sin(
            2.0 * np.pi * 50.0 * u.Hz * np.arange(0, source.shape[0]) * u.rad / self.fs).reshape(-1, 1)
        line_signal = _line_signal * line_coeff
        # generate data
        neural_var = np.var(neural_response, axis=0)
        scaled_noise = (neural_var / (self.snr * noise_var)) ** 0.5 * noise
        scaled_noise_variance = np.var(scaled_noise, axis=0) + np.finfo(float).eps * scaled_noise.unit**2.0
        data = s * (not self.return_noise_only) + scaled_noise + line_signal + eog_artifacts
        print('Theoretical RN for {:} events is {:}, and SNR {:} [dB] when target signal is included'.format(
            self.event_times.shape[0],
            np.sqrt(scaled_noise_variance / self.event_times.shape[0]),
            10 * np.log10(self.event_times.shape[0] * neural_var / scaled_noise_variance + np.finfo(float).eps)))
        events[_events_idx.astype(np.int)] = self.event_code

        self.output_node = DataNode(data=data,
                                    fs=self.fs,
                                    domain=Domain.time,
                                    layout=self.input_node.layout,
                                    )
        if self.layout_file_name is not None:
            self.output_node.apply_layout(layouts.Layout(file_name=self.layout_file_name))

        self.output_node.paths = self.input_node.paths
        self.get_events(events)
        if self.include_eog_events:
            oeg_events_with_noise = eog_waveform + line_signal[:, 0].reshape(-1, 1)
            self.output_node.append_new_channel(new_data=oeg_events_with_noise,
                                                layout_label='EOG1')

    def get_events(self, events):
        events = detect_events(event_channel=events, fs=self.output_node.fs)
        events = Events(events=np.array(events))
        for i, _code in enumerate(np.unique(events.get_events_code())):
            print('Event code:', _code, 'Number of events:', events.get_events_code(code=_code).size)
        self.output_node.events = events


class GenericInputData(InputOutputProcess):
    def __init__(self,
                 data: np.array = None,
                 fs: u.quantity.Quantity = 16384.0 * u.Hz,
                 event_times: np.array = None,
                 event_code: float = 1.0,
                 figures_path: str = None,
                 figures_subset_folder: str = '') -> InputOutputProcess:
        """
        This class allows to pass your own data without the need of having a bdf or edf file. Data will be use to
        create a compatible InoutOutputProcess that can be use straightforward in the pipeline.
        This InputOutput process takes a numpy matrix and uses it to generate a generic layout.
        :param data: numpy array (2D or 3D array; samples x channels x trials)
        :param fs: the sampling rate of the template_waveform
        :param event_times: numpy array with the timing of the events. Events are only useful when input data is a 2D
        numpy array.
        :param event_code: desired event code to be assigned to time events
        :param figures_path: path to save generated figures
        :param figures_subset_folder: string indicating a subfolder name in figures_path
        """
        super(GenericInputData, self).__init__()
        self.data = set_default_unit(copy.copy(data), u.uV)
        self.fs = set_default_unit(fs, u.Hz)
        self.event_times = set_default_unit(event_times, u.s)
        self.event_code = event_code
        self.output_node = None
        figures_path = figures_path if figures_path is not None else str(Path.home()) + '{:}'.format(sep +
                                                                                                     'pyeeg_python' +
                                                                                                     sep +
                                                                                                     'test' +
                                                                                                     sep +
                                                                                                     'figures')
        self.figures_subset_folder = figures_subset_folder

        _ch = []
        n_channels = self.data.shape[1]
        [_ch.append(ChannelItem(label='CH_{:}'.format(i), idx=i)) for i in range(n_channels)]
        layout = np.array(_ch)
        self.input_node = DataNode(fs=fs,
                                   domain=Domain.time,
                                   layout=layout,
                                   paths=DirectoryPaths(file_path=figures_path,
                                                        delete_all=False,
                                                        delete_figures=False,
                                                        figures_subset_folder=figures_subset_folder)
                                   )

    def run(self):
        events = np.array([])
        for _ev in self.event_times:
            events = np.append(events, SingleEvent(code=self.event_code,
                                                   time_pos=_ev,
                                                   dur=0))
        events = Events(events=np.array(events))

        self.output_node = DataNode(data=self.data,
                                    fs=self.fs,
                                    domain=Domain.time,
                                    layout=self.input_node.layout,
                                    )
        self.output_node.paths = self.input_node.paths
        self.output_node.events = events
