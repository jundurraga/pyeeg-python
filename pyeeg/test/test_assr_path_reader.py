import pyeeg.io.dataReadingTools as drt
import pyeeg.io.external_tools.aep_gui.group_processing_tools as assr
__author__ = 'jundurraga-ucl'

path = '/home/jundurraga/Documents/Measurements/IPD-Psycho/IPD-Angle'
data_base_path = '/home/jundurraga/Documents/Measurements/IPD-Psycho/measurements2.sqlite'
bdf_directories = drt.find_bdf_directories(path)
for c_dir in bdf_directories:
    assr.get_responses_from_path(path='',
                                 channels=list(range(66)),
                                 low_pass=60,
                                 high_pass=0.5,
                                 generate_mat_file=False,
                                 data_base_path=data_base_path,
                                 save_to_data_base=False,
                                 experiment='ipm-fr-ipd')
