"""
.. _tut-assr-phase-locking-sim:

ASSR Phase Locking Value Test Example (Simulated)
===============================
In this example we simulate an ASSR and we investigate the significant of the phase by means of a Rayleigh Test on
the frequency of interest

.. contents:: Page contents
   :local:
   :depth: 2
"""
# Enable below for interactive backend
# import matplotlib
# if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
#    matplotlib.use('Qt5Agg')
from pyeeg.processing.pipe.pype_line_definitions import *
from pyeeg.processing.pipe.definitions import GenerateInputData
from pyeeg.processing.tools.template_generator.auditory_waveforms import aep
from pyeeg.io.storage.data_storage_tools import *
import os


# %%
# Generate some data
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# We generate some auditory steady-state response (ASSR)
fs = 256.0
epoch_length = 4.0
epoch_length = np.ceil(epoch_length * fs) / fs  # fit to fs rate
assr_frequency = 41.0
assr_frequency = np.ceil(epoch_length * assr_frequency) / epoch_length  # fit to epoch length
# here we pick some random frequencies to test statistical detection
random_frequencies = np.unique(np.random.rand(10) * 30)
template_waveform, _ = aep(fs=fs)
n_channels = 32
event_times = np.arange(0, 40.0, 1 / assr_frequency)
reader = GenerateInputData(template_waveform=template_waveform,
                           fs=fs,
                           n_channels=n_channels,
                           snr=0.0001,
                           layout_file_name='biosemi32.lay',
                           event_times=event_times,
                           event_code=1.0,
                           figures_subset_folder='assr_plv_rayleigh_test')
reader.run()

# %%
# Resize events
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# Now we keep events at intervals that correspond to our desired epoch length
events = reader.output_node.events.get_events(code=1)
# skip events to preserve only those at each epoch point
_new_events = Events(events=events[0:-1:int(epoch_length * assr_frequency)])
reader.output_node.events = _new_events

# %%
# Start the pipeline
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# Some processing to obtain clean epochs
pipe_line = PipePool()
pipe_line.append(ReferenceData(reader, reference_channels=['Cz'], invert_polarity=True),
                 name='referenced')
pipe_line.append(AutoRemoveBadChannels(pipe_line.get_process('referenced')), name='channel_cleaned')
pipe_line.append(FilterData(pipe_line.get_process('channel_cleaned'), high_pass=2.0, low_pass=100.0),
                 name='time_filtered_data')
pipe_line.append(EpochData(pipe_line.get_process('time_filtered_data'),
                           event_code=1.0,
                           base_line_correction=False),
                 name='time_epochs')
pipe_line.run()

# %%
# Compute and plot components in the frequency-domain
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# Spatial filter is a applied in the frequency-domain
pipe_line.append(CreateAndApplySpatialFilter(pipe_line.get_process('time_epochs'),
                                             sf_join_frequencies=np.array([assr_frequency]),
                                             sf_components=np.arange(0, 2),
                                             components_to_plot=np.arange(0, 5),
                                             projection_domain=Domain.frequency),
                 name='dss_time_epochs')
pipe_line.run()

# %%
# Compute global field power (GFP)
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# We compute the GFP across channels and epochs. A new channel with the GFP is appended to the data

pipe_line.append(AppendGFPChannel(pipe_line.get_process('dss_time_epochs')),
                 name='dss_time_epochs_with_gfp')
pipe_line.run()

# %%
# Run Rayleigh Tests (phase-locking value)
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# Now we run an Rayleigh Tests to determine if frequency component is significantly phase-locked

pipe_line.append(PhaseLockingValue(pipe_line.get_process('dss_time_epochs_with_gfp'),
                                   n_fft=int(epoch_length*fs*1),
                                   test_frequencies=np.append(random_frequencies,
                                                              [assr_frequency, 2 * assr_frequency]),
                                   weight_data=True),
                 name='rayleigh_tests')

# %%
# Generate Phase-locking figures
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# Now we plot PLV in the frequency-domain

pipe_line.append(PlotTopographicMap(pipe_line.get_process('rayleigh_tests'),
                                    plot_x_lim=[0, 90],
                                    plot_y_lim=[0, 1.2],
                                    return_figures=True))
pipe_line.run()

# %%
# Generate current pipeline diagram
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# We generate a diagram and save it
pipe_line.diagram(file_name=reader.output_node.paths.figures_current_dir + 'pipeline.png')
pipe_line.run()

# %%
# Save results to a database
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# We get the measurements we are interested in and save them into a database
rayleigh_process = pipe_line.get_process('rayleigh_tests')

rayleigh_tests = PandasDataTable(table_name='rayleigh',
                                 pandas_df=rayleigh_process.output_node.statistical_tests)
pvl_waveform_table = PandasDataTable(table_name='pvl_waveform',
                                     pandas_df=rayleigh_process.output_node.data_to_pandas())

# now we save our data to a database
subject_info = SubjectInformation(subject_id='Test_Subject')
measurement_info = MeasurementInformation(
     date='Today',
     experiment='sim')

_parameters = {'Type': 'ASSR'}
data_base_path = reader.input_node.paths.file_directory + os.sep + 'assr_rayleigh_test_data.sqlite'
store_data(data_base_path=data_base_path,
           subject_info=subject_info,
           measurement_info=measurement_info,
           recording_info={'recording_device': 'dummy_device'},
           stimuli_info=_parameters,
           pandas_df=[rayleigh_tests, pvl_waveform_table])
