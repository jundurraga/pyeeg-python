"""
.. _tut-assr-using-own-raw-data-sim:

ASSR F-Test Example (Simulated)
===============================

In this example we simulate an ASSR and we investigate the significant of the response by means of an F test that
compares the variance of the frequency of interest with the variance of neighbouring frequency bins

.. contents:: Page contents
   :local:
   :depth: 2
"""
# Enable below for interactive backend
# import matplotlib
# if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
#    matplotlib.use('Qt5Agg')
from pyeeg.processing.pipe.pype_line_definitions import *
from pyeeg.processing.pipe.definitions import GenericInputData
from pyeeg.io.storage.data_storage_tools import *
import os


def my_pipe():
    fs = 256.0
    epoch_length = 4.0
    epoch_length = np.ceil(epoch_length * fs) / fs  # fit to fs rate
    assr_frequency = 41.0
    assr_frequency = np.ceil(epoch_length * assr_frequency) / epoch_length  # fit to epoch length
    # here we generate and pass our own data
    n_epochs = 20
    my_sin = np.sin(2 * np.pi * assr_frequency * np.arange(0, epoch_length * n_epochs, 1 / fs))
    template_waveform = np.array([my_sin, 0.5 * my_sin, 0.25 * my_sin]).T
    # add some noise
    np.random.seed(1)
    template_waveform = template_waveform + np.random.randn(*template_waveform.shape)
    event_times = np.arange(0, n_epochs) * epoch_length
    reader = GenericInputData(data=template_waveform,
                              fs=fs,
                              event_times=event_times,
                              event_code=1.0,
                              figures_subset_folder='my_own_raw_assr_f_test')
    reader.run()
    pipe_line = PipePool()
    pipe_line.append(FilterData(reader,
                                high_pass=2.0,
                                low_pass=100.0),
                     name='time_filtered_data')
    pipe_line.append(EpochData(pipe_line.get_process('time_filtered_data'),
                               event_code=1.0,
                               base_line_correction=False),
                     name='time_epochs')
    pipe_line.append(CreateAndApplySpatialFilter(pipe_line.get_process('time_epochs'),
                                                 sf_join_frequencies=np.array([assr_frequency,
                                                                               2 * assr_frequency]),
                                                 components_to_plot=np.arange(0, 5),
                                                 projection_domain=Domain.frequency),
                     name='dss_time_epochs')
    pipe_line.run()
    pipe_line.append(AppendGFPChannel(pipe_line.get_process('dss_time_epochs')),
                     name='dss_time_epochs_with_gfp')
    pipe_line.append(AppendGFPChannel(pipe_line.get_process('time_epochs')),
                     name='time_epochs_with_gfp')

    pipe_line.append(AverageEpochs(pipe_line.get_process('dss_time_epochs_with_gfp')),
                     name='time_ave_dss')
    pipe_line.append(AverageEpochs(pipe_line.get_process('time_epochs_with_gfp')),
                     name='time_ave')

    pipe_line.append(FTest(pipe_line.get_process('time_ave_dss'),
                           n_fft=int(epoch_length*fs*1),
                           test_frequencies=np.array([assr_frequency]),
                           delta_frequency=9.0),
                     name='fft_ave_f_tests_dss')
    pipe_line.append(FTest(pipe_line.get_process('time_ave'),
                           n_fft=int(epoch_length * fs * 1),
                           test_frequencies=np.array([assr_frequency]),
                           delta_frequency=9.0),
                     name='fft_ave_f_tests')

    pipe_line.append(PlotTopographicMap(pipe_line.get_process('fft_ave_f_tests_dss'),
                                        plot_x_lim=[0, 90],
                                        plot_y_lim=[0, 4],
                                        return_figures=True,
                                        user_naming_rule='dss'))
    pipe_line.append(PlotTopographicMap(pipe_line.get_process('fft_ave_f_tests'),
                                        times=np.array([0, 1/assr_frequency]),
                                        plot_x_lim=[0, 90],
                                        plot_y_lim=[0, 4],
                                        return_figures=True,
                                        user_naming_rule='standard'))
    pipe_line.append(PlotWaveforms(pipe_line.get_process('fft_ave_f_tests'),
                                   ch_to_plot=np.array(['CH_0']),
                                   overlay=[pipe_line.get_process('fft_ave_f_tests_dss')],
                                   plot_x_lim=[0, 90],
                                   show_following_stats=['frequency_tested', 'f'],
                                   return_figures=True,
                                   user_naming_rule='waveforms',
                                   fig_format='.pdf'))
    pipe_line.diagram(file_name=reader.output_node.paths.figures_current_dir + 'pipeline.png')
    pipe_line.run()

    freq_process = pipe_line.get_process('fft_ave_f_tests')

    f_tests = PandasDataTable(table_name='f_test',
                              pandas_df=freq_process.output_node.statistical_tests)
    waveform_table = PandasDataTable(table_name='fft_ave_waveform',
                                     pandas_df=freq_process.output_node.data_to_pandas())

    # now we save our data to a database
    subject_info = SubjectInformation(subject_id='Test_Subject')
    measurement_info = MeasurementInformation(
         date='Today',
         experiment='sim')
    _parameters = {'Type': 'ASSR'}
    data_base_path = reader.input_node.paths.file_directory + os.sep + 'assr_own_raw_data_f_test_data.sqlite'
    store_data(data_base_path=data_base_path,
               subject_info=subject_info,
               measurement_info=measurement_info,
               recording_info={'recording_device': 'dummy_device'},
               stimuli_info=_parameters,
               pandas_df=[f_tests, waveform_table])


if __name__ == "__main__":
    my_pipe()
