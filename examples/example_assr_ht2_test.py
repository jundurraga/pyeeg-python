"""
.. _tut-assr-ht2-test-sim:

ASSR Hotelling-T2 test (Simulated)
==================================

In this example we simulate an ASSR and we assess its significance using the Hotelling's T2 test.
This method uses the real and imaginary part of the frequency bin of interest to assess whether the points from
each epoch are significantly different from zero (the center of the polar complex-plane)

.. contents:: Page contents
   :local:
   :depth: 2
"""
# Enable below for interactive backend
# import matplotlib
# if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
#    matplotlib.use('Qt5Agg')
from pyeeg.processing.pipe.pype_line_definitions import *
from pyeeg.processing.pipe.definitions import GenerateInputData
from pyeeg.processing.tools.template_generator.auditory_waveforms import aep
from pyeeg.io.storage.data_storage_tools import *
import os

# %%
# Generate some data
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# We generate some auditory steady-state response (ASSR)
fs = 256.0
epoch_length = 4.0
epoch_length = np.ceil(epoch_length * fs) / fs  # fit to fs rate
assr_frequency = np.array([41.0])
assr_frequency = np.ceil(epoch_length * assr_frequency) / epoch_length  # fit to epoch length
# here we pick some random frequencies to test statistical detection
random_frequencies = np.unique(np.random.rand(2)*30)
template_waveform, _ = aep(fs=fs)
n_channels = 16
event_times = np.arange(0, 360.0, 1/assr_frequency)
reader = GenerateInputData(template_waveform=template_waveform,
                           fs=fs,
                           n_channels=n_channels,
                           snr=0.05,
                           layout_file_name='biosemi32.lay',
                           event_times=event_times,
                           event_code=1.0,
                           figures_subset_folder='assr_ht2_test',
                           noise_seed=0)
reader.run()

# %%
# Resize events
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# Now we keep events at intervals that correspond to our desired epoch length
events = reader.output_node.events.get_events(code=1)
# skip events to preserve only those at each epoch point
_new_events = Events(events=events[0:-1:int(epoch_length * assr_frequency)])
reader.output_node.events = _new_events

# %%
# Start the pipeline
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# Some processing to obtain clean epochs

pipe_line = PipePool()
pipe_line.append(ReferenceData(reader,
                               reference_channels=['Cz'],
                               invert_polarity=True),
                 name='referenced')
pipe_line.append(AutoRemoveBadChannels(pipe_line.get_process('referenced')),
                 name='channel_cleaned')
pipe_line.append(FilterData(pipe_line.get_process('channel_cleaned'),
                            high_pass=2.0,
                            low_pass=100.0),
                 name='time_filtered_data')
pipe_line.append(EpochData(pipe_line.get_process('time_filtered_data'),
                           event_code=1.0,
                           base_line_correction=False),
                 name='time_epochs')
pipe_line.run()

# %%
# Plot components in the frequency-domain
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# Spatial filter is a applied in the frequency-domain

pipe_line.append(CreateAndApplySpatialFilter(pipe_line.get_process('time_epochs'),
                                             sf_join_frequencies=assr_frequency,
                                             projection_domain=Domain.frequency,
                                             block_size=10,
                                             return_figures=True,
                                             delta_frequency=2 * u.Hz,
                                             plot_y_lim=[0, 5]),
                 name='dss_time_epochs')
pipe_line.run()

# %%
# Compute global field power (GFP)
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# We compute the GFP across channels and epochs. A new channel with the GFP is appended to the data

pipe_line.append(AppendGFPChannel(pipe_line.get_process('dss_time_epochs')),
                 name='dss_time_epochs_with_gfp')
pipe_line.run()


# %%
# Average epochs
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# We compute the average and simultaneously get statistical tests on the test_frequencies

pipe_line.append(AverageEpochsFrequencyDomain(pipe_line.get_process('dss_time_epochs_with_gfp'),
                                              n_fft=int(epoch_length*fs),
                                              weight_frequencies=assr_frequency,
                                              test_frequencies=np.concatenate((
                                                  assr_frequency,
                                                  2 * assr_frequency,
                                                  random_frequencies)),
                                              delta_frequency=2 * u.Hz),
                 name='fft_ave')
pipe_line.append(AverageEpochsFrequencyDomain(pipe_line.get_process('time_epochs'),
                                              n_fft=int(epoch_length * fs),
                                              weight_frequencies=assr_frequency,
                                              test_frequencies=np.concatenate((
                                                  assr_frequency,
                                                  2 * assr_frequency,
                                                  random_frequencies)),
                                              delta_frequency=2 * u.Hz),
                 name='std_fft_ave')
pipe_line.run()
pipe_line.get_process('fft_ave').output_node.statistical_tests

# %%
# Generate figures
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# Now we run plot the average waveforms and show the stats

pipe_line.append(PlotWaveforms(pipe_line.get_process('std_fft_ave'),
                               overlay=[pipe_line.get_process('fft_ave')],
                               plot_x_lim=[0, 90],
                               ch_to_plot=np.array(['O2', 'T8', 'T7']),
                               show_following_stats=['f'],
                               return_figures=True,
                               user_naming_rule='standard_and_dss'))

pipe_line.append(PlotTopographicMap(pipe_line.get_process('fft_ave'),
                                    plot_x_lim=[0, 90],
                                    plot_y_lim=[0, 6],
                                    return_figures=True))

# %%
# Generate current pipeline diagram
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# We generate a diagram and save it
pipe_line.diagram(file_name=reader.output_node.paths.figures_current_dir + 'pipeline.png')
pipe_line.run()

# %%
# Save results to a database
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# We get the measurements we are interested in and save them into a database

freq_process = pipe_line.get_process('fft_ave')
hotelling_table = PandasDataTable(table_name='hotelling_test',
                                  pandas_df=freq_process.output_node.statistical_tests)
waveform_table = PandasDataTable(table_name='frequency_average_data',
                                 pandas_df=freq_process.output_node.data_to_pandas())

# now we save our data to a database
subject_info = SubjectInformation(subject_id='Test_Subject')
measurement_info = MeasurementInformation(
    date='Today',
    experiment='sim')

_parameters = {'Type': 'ASSR'}
data_base_path = reader.input_node.paths.file_directory + os.sep + 'assr_ht2_test.sqlite'
store_data(data_base_path=data_base_path,
           subject_info=subject_info,
           measurement_info=measurement_info,
           recording_info={'recording_device': 'dummy_device'},
           stimuli_info=_parameters,
           pandas_df=[hotelling_table, waveform_table])
