"""
.. _tut-abr-sim:

ABR peak detection (Simulated)
==============================

This example simulates an auditory brainstem response, detect peaks, and save results to a database.
.. contents:: Page contents
   :local:
   :depth: 2
"""
# Enable below for interactive backend
# import matplotlib
# if 'Qt5Agg' in matplotlib.rcsetup.all_backends:
#    matplotlib.use('Qt5Agg')

from pyeeg.processing.pipe.pype_line_definitions import *
from pyeeg.processing.pipe.definitions import GenerateInputData
from pyeeg.processing.tools.roi.definitions import TimeROI
from pyeeg.processing.tools.template_generator.auditory_waveforms import abr
from pyeeg.io.storage.data_storage_tools import *
import os


# %%
# Time windows
# ------------------------
# Time windows are used to define where to look for different peaks
# 'code block'.
tw = np.array([
    TimePeakWindow(ini_time=4e-3 * u.s, end_time=6e-3 * u.s, label='V', positive_peak=True),
    TimePeakWindow(ini_ref='V', end_time=12e-3 * u.s, label='N_V', positive_peak=False),
    TimePeakWindow(ini_time=4e-3 * u.s, end_ref='V', label='N_III', positive_peak=False),
    TimePeakWindow(ini_time=3e-3 * u.s, end_ref='N_III', label='III', positive_peak=True),
    TimePeakWindow(ini_time=2e-3 * u.s, end_ref='III', label='N_II', positive_peak=False),
    TimePeakWindow(ini_time=2e-3 * u.s, end_ref='N_II', label='II', positive_peak=True),
    TimePeakWindow(ini_time=1e-3 * u.s, end_ref='II', label='N_I', positive_peak=False),
    TimePeakWindow(ini_time=1e-3 * u.s, end_ref='N_I', label='I', positive_peak=True),
])
# %%
# Peak-to-peak Measures
# ------------------------
# Peak-to-peak measures are defined based on the labels of the TimePeaks defined above.
# 'code block'.
pm = np.array([
    PeakToPeakMeasure(ini_peak='I', end_peak='N_I'),
    PeakToPeakMeasure(ini_peak='II', end_peak='N_II'),
    PeakToPeakMeasure(ini_peak='III', end_peak='N_III'),
    PeakToPeakMeasure(ini_peak='V', end_peak='N_V'),
    PeakToPeakMeasure(ini_peak='III', end_peak='V')])
# %%
# Time regions of interest
# ------------------------
# TimeROI are defined as time regions where different measures will be performed, e.g. SNR measure or statistical
# measures
# 'code block'.
roi_windows = np.array([TimeROI(ini_time=1e-3 * u.s, end_time=1./47 * u.s, measure="snr", label="abr_snr")])

# %%
# Generating some data
# ------------------------
# First we generate a target signal, in this example, and auditory brainstem response (ABR) with a target final SNR,
# this is, the expected SNR when all trials or epochs are averaged.
# 'code block'.

fs = 8192.0 * u.Hz
template_waveform, _ = abr(fs=fs, time_length=0.02 * u.s)
n_channels = 4
event_times = np.arange(0, 240.0, 1./47.0) * u.s
n_trials = event_times.shape[0]
desired_snr_db = 20
snr = 10 ** (desired_snr_db / 10) / n_trials

reader = GenerateInputData(template_waveform=template_waveform,
                           fs=fs,
                           n_channels=n_channels,
                           mixing_matrix=np.array([0.001, 1, 1, 1]),
                           snr=snr,
                           include_eog_events=True,
                           event_times=event_times,
                           event_code=1.0,
                           figures_subset_folder='abr_test')
reader.run()
# %%
# Processing pipe line
# ------------------------
# First we initialize and populate the pipeline

pipe_line = PipePool()

pipe_line.append(ReferenceData(reader,
                               reference_channels=['CH_0'],
                               invert_polarity=False),
                 name='referenced')
pipe_line.append(RegressOutEOG(pipe_line.get_process('referenced'),
                               ref_channel_labels=['EOG1'],
                               method='template'),
                 name='channel_cleaned')
pipe_line.append(FilterData(pipe_line.get_process('channel_cleaned'),
                            high_pass=47.0 * u.Hz,
                            low_pass=None),
                 name='time_filtered_data')
pipe_line.run()
# %%
# Lets see the output of the filter
# ------------------------
# First we generate a target signal, in this example, and auditory brainstem response (ABR).

pipe_line.get_process('time_filtered_data').plot_input_output(plot_input=True,
                                                              plot_output=True)

# %%
# Processing pipe continue
# ------------------------
# First we generate a target signal, in this example, and auditory brainstem response (ABR).

pipe_line.append(EpochData(pipe_line.get_process('time_filtered_data'),
                           event_code=1.0,
                           base_line_correction=False),
                 name='time_epochs_filtered')
pipe_line.append(RejectEpochs(pipe_line.get_process('time_epochs_filtered'),
                              rejection_threshold=100 * u.uV,
                              rejection_percentage=0.1,
                              std_threshold=2.0),
                 name='time_epochs')
pipe_line.append(CreateAndApplySpatialFilter(pipe_line.get_process('time_epochs'),
                                             plot_x_lim=[0, 0.012],
                                             sf_components=np.arange(0, 1),
                                             return_figures=True),
                 name='dss_time_epochs')
pipe_line.append(HotellingT2Test(pipe_line.get_process('time_epochs'),
                                 roi_windows=roi_windows,
                                 block_time=1e-3 * u.s),
                 name='ht2')
pipe_line.append(AverageEpochs(pipe_line.get_process('time_epochs'),
                               roi_windows=roi_windows),
                 name='time_average')
pipe_line.append(AverageEpochs(pipe_line.get_process('dss_time_epochs'),
                               roi_windows=roi_windows),
                 name='time_average_dss')
# we up-sample to improve time peak detection
pipe_line.append(ReSampling(pipe_line.get_process('time_average'),
                            new_sampling_rate=fs * 2),
                 name='up_sampled')
pipe_line.append(ReSampling(pipe_line.get_process('time_average_dss'),
                            new_sampling_rate=fs * 2),
                 name='up_sampled_dss')
pipe_line.run()
pipe_line.get_process('up_sampled_dss').plot_input_output(plot_input=True,
                                                          plot_output=True)
pipe_line.append(PeakDetectionTimeDomain(pipe_line.get_process('up_sampled'),
                                         time_peak_windows=tw,
                                         peak_to_peak_measures=pm),
                 name="up_sampled_peaks")
pipe_line.append(PeakDetectionTimeDomain(pipe_line.get_process('up_sampled_dss'),
                                         time_peak_windows=tw,
                                         peak_to_peak_measures=pm),
                 name="up_sampled_peaks_dss")
pipe_line.append(PlotWaveforms(pipe_line.get_process('up_sampled_peaks'),
                               overlay=[pipe_line.get_process('up_sampled_peaks_dss')],
                               plot_x_lim=[0, 0.012],
                               return_figures=True))

pipe_line.diagram(file_name=reader.output_node.paths.figures_current_dir + 'pipeline.png')
pipe_line.run()

time_measures = pipe_line.get_process('up_sampled_peaks')
time_table = PandasDataTable(table_name='time_peaks',
                             pandas_df=time_measures.output_node.peak_times)
amps_table = PandasDataTable(table_name='amplitudes',
                             pandas_df=time_measures.output_node.peak_to_peak_amplitudes)

time_waveforms = pipe_line.get_process('up_sampled_peaks')
waveform_table = PandasDataTable(table_name='time_waveforms',
                                 pandas_df=time_waveforms.output_node.data_to_pandas())

ht2_tests = pipe_line.get_process('ht2')
h_test_table = PandasDataTable(table_name='h_t2_test',
                               pandas_df=ht2_tests.output_node.statistical_tests)

time_average = pipe_line.get_process('time_average')
fpm_table = PandasDataTable(table_name='fpm_table',
                            pandas_df=time_average.output_node.statistical_tests)
# now we save our data to a database
subject_info = SubjectInformation(subject_id='Test_Subject')
measurement_info = MeasurementInformation(
    date='Today',
    experiment='sim')

_parameters = {'Type': 'ABR'}
data_base_path = reader.input_node.paths.file_directory + os.sep + 'abr_test_data.sqlite'
store_data(data_base_path=data_base_path,
           subject_info=subject_info,
           measurement_info=measurement_info,
           recording_info={'recording_device': 'dummy_device'},
           stimuli_info=_parameters,
           pandas_df=[time_table, amps_table, waveform_table, h_test_table, fpm_table])

