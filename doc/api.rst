:orphan:

API
============

.. currentmodule:: pyeeg

.. toctree::

Experimental Design and Analysis
--------------------------------

.. currentmodule:: pyeeg.processing.tools.eeg_epoch_operators

.. automodule:: pyeeg.processing.tools.eeg_epoch_operators
   :no-members:
   :no-inherited-members:

.. autosummary::
   :toctree: generated/

   et_covariance

