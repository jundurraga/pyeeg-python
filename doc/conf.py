import multiprocessing as mp
import sys
from distutils.version import LooseVersion
import sphinx
import os
sys.path.append("../")
from pyeeg import io  # noqa: E402

# If extensions (or modules to document with autodoc) are in another directory,

# add these directories to sys.path here. If the directory is relative to the

# documentation root, use os.path.abspath to make it absolute, like shown here.

# sys.path.insert(0, os.path.abspath('.'))

# -- General configuration ------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.

# needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be

# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom

# ones.

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx_gallery.gen_gallery',
    # 'numpydoc'
]
autosummary_generate = True

# sphinx-gallery configuration
sphinx_gallery_conf = {
    'doc_module': 'pyeeg',
    'backreferences_dir': os.path.join('generated'),
    'reference_url': {'pyeeg-python': None},
    'download_all_examples': False,
    'show_memory': True,
    'filename_pattern': '/example_',
    'capture_repr': ('_repr_html_', '__repr__')
    # 'autoDocstring.docstringFormat': "sphinx"
}
# Generate the plots for the gallery
plot_gallery = True
